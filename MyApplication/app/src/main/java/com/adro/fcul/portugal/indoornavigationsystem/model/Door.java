package com.adro.fcul.portugal.indoornavigationsystem.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by adroa on 2017-07-13.
 */

@Getter
@Setter
public class Door {

    private Integer id;
    private PointOfInterest pointOfInterestId;
}
