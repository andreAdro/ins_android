package com.adro.fcul.portugal.indoornavigationsystem.conectivity;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by adroa on 2017-07-09.
 */

public interface OnJsonResponseCallBack {
    void onJsonResponse(boolean success, JSONArray response);
    void onJsonResponse(boolean success, JSONObject response);
}
