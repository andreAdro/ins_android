package com.adro.fcul.portugal.indoornavigationsystem.conectivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.adro.fcul.portugal.indoornavigationsystem.variables.Variables.REQUEST_CODE_ALL_ROOMS;
import static com.adro.fcul.portugal.indoornavigationsystem.variables.Variables.ROOMS_ENDPOINT;

/**
 * Created by adroa on 2017-07-13.
 */

public class RoomDataActivity extends AppCompatActivity {
    private String TAG = "RoomDataActiviy";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        internalRouting();
    }

    private void internalRouting() {
        String reqCode = getIntent().getExtras().getString("REQUEST_CODE");

        switch (reqCode) {
            case REQUEST_CODE_ALL_ROOMS:
                getAllRooms();
                break;
            default:
                finishActivityWithError();
                break;
        }
    }

    private void finishActivityWithError() {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }


    private void getAllRooms() {
        Log.d(TAG, "getRooms() Started..");
        try {
            ApiService roomsService = new ApiService(ROOMS_ENDPOINT);
            roomsService.connectToServiceGet(new OnJsonResponseCallBack() {
                @Override
                public void onJsonResponse(boolean success, JSONArray response) {
                    if (success) {
                        Log.d(TAG, "getAllRooms() Callback with JSONArray. SUCCESS.");

                        Intent returnIntent = getIntent();
                        returnIntent.putExtra("RESULT", response.toString());
                        returnIntent.putExtra("REQUEST_CODE", REQUEST_CODE_ALL_ROOMS);

                        setResult(Integer.valueOf(REQUEST_CODE_ALL_ROOMS), returnIntent);
                        finish();

                    } else {
                        Log.d(TAG, "onJsonResponse: NOT SUCCESS. Something went wrong while calling service..");
                        finishActivityWithError();
                    }
                }

                @Override
                public void onJsonResponse(boolean success, JSONObject response) {
                    if (success) {
                        Log.d(TAG, "getAllRooms() Callback with JSONObject. SUCCESS.");

                        Intent returnIntent = getIntent();
                        returnIntent.putExtra("RESULT", response.toString());
                        returnIntent.putExtra("REQUEST_CODE", REQUEST_CODE_ALL_ROOMS);

                        setResult(Integer.valueOf(REQUEST_CODE_ALL_ROOMS), returnIntent);
                        finish();

                    } else {
                        Log.d(TAG, "onJsonResponse: JSONObject called. Wrong output.");
                        finishActivityWithError();
                    }
                }
            });
        } catch (JSONException e) {
            Log.d(TAG, "UNEXPECTED EXCEPTION");
            Log.d(TAG, "connectToServiceGet: JsonException => " + e);
            Log.d(TAG, "UNEXPECTED EXCEPTION");
        }
        Log.d(TAG, "getRooms() Finished.");
    }
}
