package com.adro.fcul.portugal.indoornavigationsystem.model;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by adroa on 2017-07-02.
 */

@Setter
@Getter
@ToString
@NoArgsConstructor
public class Room implements Serializable {

    private Integer id;
    private String name;
    private Integer floor;
    private ArrayList<Vertice> vertices;
    private ArrayList<Door> doors;

}