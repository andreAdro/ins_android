package com.adro.fcul.portugal.indoornavigationsystem.variables;

/**
 * Created by adroa on 2017-07-09.
 */

public class Variables {

    public static final String ROOMS_ENDPOINT = "rooms";
    public static final String ROUTE_ENDPOINT = "route";
    public static final String ROUTE_SOURCE_PARAM = "source";
    public static final String ROUTE_DESTINATION_PARAM = "destination";


    public static final String REQUEST_CODE_ALL_ROOMS = "1";
    public static final String REQUEST_CODE_SEARCH = "2";
    public static final String REQUEST_CODE_ROUTE = "3";

}
