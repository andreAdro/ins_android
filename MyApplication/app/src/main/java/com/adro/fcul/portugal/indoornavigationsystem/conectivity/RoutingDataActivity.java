package com.adro.fcul.portugal.indoornavigationsystem.conectivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.adro.fcul.portugal.indoornavigationsystem.model.Position;
import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.adro.fcul.portugal.indoornavigationsystem.variables.Variables.REQUEST_CODE_ROUTE;
import static com.adro.fcul.portugal.indoornavigationsystem.variables.Variables.ROUTE_DESTINATION_PARAM;
import static com.adro.fcul.portugal.indoornavigationsystem.variables.Variables.ROUTE_ENDPOINT;
import static com.adro.fcul.portugal.indoornavigationsystem.variables.Variables.ROUTE_SOURCE_PARAM;

/**
 * Created by adroa on 2017-07-09.
 */

public class RoutingDataActivity extends AppCompatActivity {
    private static final String TAG = "RoutingDataActivity";

    private Position beginning;
    private Position destination;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        internalRouting();
        getRouteFromService();
    }

    private void internalRouting() {
        String reqCode = getIntent().getExtras().getString("REQUEST_CODE");

        switch (reqCode) {
            case REQUEST_CODE_ROUTE:
                getStartAndFinishPositions();
                getRouteFromService();
                break;
            default:
                finishActivityWithError();
                break;
        }
    }

    private void finishActivityWithError() {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    private void getStartAndFinishPositions() {
        beginning = new Gson().fromJson(getIntent().getExtras().getString("BEGINNING"), Position.class);
        destination = new Gson().fromJson(getIntent().getExtras().getString("DESTINATION"), Position.class);
    }


    private void getRouteFromService() {
        Log.d(TAG, "getRouteFromService() Started..");
        try {
            ApiService roomsService = new ApiService(ROUTE_ENDPOINT);

            RequestParams params = buildParams();
            roomsService.setRequestParams(params);

            roomsService.connectToServiceGet(new OnJsonResponseCallBack() {
                @Override
                public void onJsonResponse(boolean success, JSONArray response) {
                    if (success) {
                        Log.d(TAG, "getRouteFromService() Callback with JSONArray. SUCCESS. Converting JSONArray to Java Object..");

                        Intent returnIntent = getIntent();
                        returnIntent.putExtra("RESULT", response.toString());
                        returnIntent.putExtra("REQUEST_CODE", REQUEST_CODE_ROUTE);
                        setResult(Integer.valueOf(REQUEST_CODE_ROUTE), returnIntent);

                        finish();

                        Log.d(TAG, "getRouteFromService() Callback with JSONArray. SUCCESS. Converting JSONArray to Java Object. SUCCESS.");

                    } else {
                        Log.d(TAG, "getRouteFromService: NOT SUCCESS. Something went wrong while calling service..");
                        //TODO handle not success
                    }
                }

                @Override
                public void onJsonResponse(boolean success, JSONObject response) {
                    if (success) {
                        Log.d(TAG, "onJsonResponse: JSONObject called. Wrong output.");
                    } else {
                        Log.d(TAG, "onJsonResponse: JSONObject called. Wrong output.");
                    }
                }
            });
        } catch (JSONException e) {
            Log.d(TAG, "UNEXPECTED EXCEPTION");
            Log.d(TAG, "connectToServiceGet: JsonException => " + e);
            Log.d(TAG, "UNEXPECTED EXCEPTION");
        }
        Log.d(TAG, "getRouteFromService() Finished.");

    }

    private RequestParams buildParams() {
        RequestParams params = new RequestParams();

        String firstValue = beginning.getLat().toString() + ";" + beginning.getLng().toString();
        String secondValue = destination.getLat().toString() + ";" + destination.getLng().toString();

        params.add(ROUTE_SOURCE_PARAM, firstValue);
        params.add(ROUTE_DESTINATION_PARAM, secondValue);

        return params;
    }
}
