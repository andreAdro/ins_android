package com.adro.fcul.portugal.indoornavigationsystem.model;

import com.mapbox.mapboxsdk.geometry.LatLng;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by adroa on 2017-07-09.
 */

@Getter
@Setter
@NoArgsConstructor
public class Position implements Serializable {
    private static final long serialVersionUID = 949340114020851425L;

    private Integer id;
    private Double lat;
    private Double lng;
    private String user;
    private Timestamp timestamp;

    public Position(LatLng latLng){
        lat = latLng.getLatitude();
        lng = latLng.getLongitude();
    }

    public LatLng getLatLngObj() {
        return new LatLng(lat,lng);
    }


    @Override
    public String toString(){
        return lat + ";" + lng;
    }

}
