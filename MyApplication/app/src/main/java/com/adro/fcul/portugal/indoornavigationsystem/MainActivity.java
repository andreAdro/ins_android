package com.adro.fcul.portugal.indoornavigationsystem;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.adro.fcul.portugal.indoornavigationsystem.conectivity.RoomDataActivity;
import com.adro.fcul.portugal.indoornavigationsystem.model.Position;
import com.adro.fcul.portugal.indoornavigationsystem.model.Room;
import com.adro.fcul.portugal.indoornavigationsystem.model.Vertice;
import com.adro.fcul.portugal.indoornavigationsystem.conectivity.RoutingDataActivity;
import com.adro.fcul.portugal.indoornavigationsystem.variables.Variables;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Polygon;
import com.mapbox.mapboxsdk.annotations.PolygonOptions;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.constants.Style;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import static com.adro.fcul.portugal.indoornavigationsystem.variables.Variables.REQUEST_CODE_ROUTE;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "MainActivity";
    private MapView mapView;

    private List<Room> rooms;
    private PolygonOptions selectedRoom;
    private LinkedHashMap<String, PolygonOptions> roomPolygons = new LinkedHashMap<>();

    // TODO default value, should be replaced by get current position
    private Position beginning = new Position(new LatLng(38.7569731, -9.1573160));
    private Position destination = new Position(new LatLng(38.7569731, -9.1573160));
    private List<LatLng> route = new ArrayList<>();
    private PolylineOptions currentRoute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        lauchDataActivities();

        buildMap(savedInstanceState);

        buildSlidingBar();

    }

    private void buildMap(Bundle savedInstanceState) {
        Mapbox.getInstance(this, getString(R.string.access_token));
        setContentView(R.layout.activity_main);

        // Create a mapView
        mapView = (MapView) findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);

        // Add a MapboxMap
        this.mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {

                // Customize map with markers, polylines, etc.
                mapboxMap.setStyleUrl("mapbox://styles/dotadro/cj3yw0xb127392rnt4v1g3hvo");
                mapboxMap.setStyleUrl(Style.LIGHT);

                LatLng latLng = new LatLng();
                latLng.setLatitude(beginning.getLat());
                latLng.setLongitude(beginning.getLng());

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(latLng)
                        .zoom(19)
                        .tilt(50)
                        .bearing(67)
                        .build();

                mapboxMap.setCameraPosition(cameraPosition);
            }
        });
    }

    private void buildSlidingBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.search) {
            Log.d(TAG, "onNavigationItemSelected: Button clicked, calling service ");
            lauchSearchActivity();

        } else if (id == R.id.wc) {

        } else if (id == R.id.bar) {

        } else if (id == R.id.atm) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void lauchDataActivities() {
        Intent roomIntent = new Intent(this, RoomDataActivity.class);
        roomIntent.putExtra("REQUEST_CODE", Variables.REQUEST_CODE_ALL_ROOMS);
        startActivityForResult(roomIntent, Integer.valueOf(Variables.REQUEST_CODE_ALL_ROOMS));
        // TODO more data regarding application "persistence"
    }

    private void lauchSearchActivity() {
        Intent searchIntent = new Intent(this, SearchActivity.class);
        searchIntent.putExtra("REQUEST_CODE", Variables.REQUEST_CODE_SEARCH);
        searchIntent.putExtra("ROOMS_DATA", roomsToJson());
        startActivityForResult(searchIntent, Integer.valueOf(Variables.REQUEST_CODE_SEARCH));

    }

    private void lauchRouteActivity() {
        Intent routeIntent = new Intent(this, RoutingDataActivity.class);
        routeIntent.putExtra("REQUEST_CODE", REQUEST_CODE_ROUTE);
        routeIntent.putExtra("BEGINNING", new Gson().toJson(beginning, Position.class));
        routeIntent.putExtra("DESTINATION", new Gson().toJson(destination, Position.class));
        startActivityForResult(routeIntent, Integer.valueOf(REQUEST_CODE_ROUTE));
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String result = "";
        if (data != null) {
            result = data.getExtras().getString("RESULT");
        }
        String reqCode = data.getExtras().getString("REQUEST_CODE");

        switch (reqCode) {
            case Variables.REQUEST_CODE_ALL_ROOMS:
                resultToRooms(result);
                drawRooms();
                break;

            case Variables.REQUEST_CODE_SEARCH:
                Log.d(TAG, "onActivityResult: ");
                Room room = new Gson().fromJson(result, Room.class);
                setDestinationByRoom(room);
                selectRoom(room.getName());
//                drawRooms();

                lauchRouteActivity();
                break;

            case REQUEST_CODE_ROUTE:
                // TODO handle response from route class
                resultToRoute(result);
                drawRoute();
                break;

            default:
                Log.d(TAG, "onActivityResult: SOMETHING WENT TERRIBLE WRONG! HANDLE IT!!!");
                break;

        }
    }

    private void resultToRooms(String resultData) {
        Type listType = new TypeToken<ArrayList<Room>>() {
        }.getType();
        rooms = new Gson().fromJson(resultData.toString(), listType);
    }

    private String roomsToJson() {
        return new Gson().toJson(rooms);
    }

    private void resultToRoute(String resultData) {
        Type listType = new TypeToken<ArrayList<Position>>() {
        }.getType();
        List<Position> positions = new Gson().fromJson(resultData.toString(), listType);

        route.clear();

        for (Position position : positions){
            LatLng latLng = new LatLng();
            latLng.setLatitude(position.getLat());
            latLng.setLongitude(position.getLng());
            route.add(latLng);
        }
    }

    private void drawRooms() {
        this.mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {

                if (rooms != null && rooms.size() > 0) {
                    List<PolygonOptions> polygons = new ArrayList<>();

                    for (Room room : rooms) {
                        PolygonOptions polygon = new PolygonOptions();
                        polygon.fillColor(Color.BLUE);

                        for (Vertice vertice : room.getVertices()) {
                            Double lat = vertice.getLat();
                            Double lng = vertice.getLng();

                            LatLng point = new LatLng(lat, lng);

                            polygon.add(point);
                        }
                        // reference to same polygon in different structures
                        polygons.add(polygon);
                        roomPolygons.put(room.getName(), polygon);

                    }
                    mapboxMap.addPolygons(polygons);
                }
            }
        });
    }

    private void selectRoom(final String roomName) {
        this.mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {

                PolygonOptions roomPolygon = roomPolygons.get(roomName);

                for (Polygon polygon : mapboxMap.getPolygons()) {
                    if (selectedRoom != null && polygon.equals(selectedRoom.getPolygon()))
                        polygon.setFillColor(Color.BLUE);

                    if (polygon.equals(roomPolygon.getPolygon()))
                        polygon.setFillColor(Color.BLACK);

                }
                selectedRoom = roomPolygon;

            }
        });
    }

    private void drawRoute() {
        this.mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                if (currentRoute!= null)
                    currentRoute.getPolyline().remove();

                if (route != null && route.size() > 0) {
                    currentRoute = new PolylineOptions();
                    currentRoute.addAll(route);
                    currentRoute.width(3);
                    currentRoute.color(Color.RED);
                    mapboxMap.addPolyline(currentRoute);
                }

            }
        });
    }

    private void setDestinationByRoom(Room room) {
        Position doorPosition = new Position();
        doorPosition.setLat(room.getDoors().get(0).getPointOfInterestId().getLat());
        doorPosition.setLng(room.getDoors().get(0).getPointOfInterestId().getLng());

        destination = doorPosition;
    }


    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }


}
