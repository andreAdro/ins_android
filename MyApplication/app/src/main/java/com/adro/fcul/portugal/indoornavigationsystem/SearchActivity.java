package com.adro.fcul.portugal.indoornavigationsystem;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.adro.fcul.portugal.indoornavigationsystem.databinding.RowItemBinding;
import com.adro.fcul.portugal.indoornavigationsystem.databinding.SearchMainBinding;
import com.adro.fcul.portugal.indoornavigationsystem.model.Room;
import com.adro.fcul.portugal.indoornavigationsystem.variables.Variables;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by adroa on 2017-07-06.
 */

public class SearchActivity extends AppCompatActivity {
    private static final String TAG = "SearchActivity";

    private SearchMainBinding mSearchMainBinding;
    private RowItemBinding mRowItemBinding;
    private RoomAdapter mRoomAdapter;

    ListView listView;
    ArrayList<Room> rooms = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: inside SearchActivity");

        mSearchMainBinding = DataBindingUtil.setContentView(this, R.layout.search_main);

        mSearchMainBinding.search.setQuery("", false);
        mSearchMainBinding.search.clearFocus();
        mSearchMainBinding.search.setQueryHint("Type room number");
        mSearchMainBinding.search.onActionViewExpanded();
        mSearchMainBinding.search.setIconified(false);

        internalRouting();

        setSearchListAdapter();

        if (listView != null)
            setListViewInvisible();
    }

    private void internalRouting() {
        String reqCode = getIntent().getExtras().getString("REQUEST_CODE");

        switch (reqCode) {
            case Variables.REQUEST_CODE_SEARCH:
                Intent passedIntent = getIntent();
                String roomsData = passedIntent.getExtras().getString("ROOMS_DATA");

                dataToObject(roomsData);
                break;
            default:
                finishActivityWithError();
                break;
        }
    }

    private void dataToObject(String roomsData) {
        Type listType = new TypeToken<ArrayList<Room>>() {
        }.getType();
        rooms = new Gson().fromJson(roomsData, listType);
    }

    private void finishActivityWithError() {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    private void setSearchListAdapter() {
        mRoomAdapter = new RoomAdapter(rooms);
        mSearchMainBinding.listView.setAdapter(mRoomAdapter);

        setOnQueryTextListener();
        setOnItemClickListener();

    }

    private void setOnQueryTextListener() {
        mSearchMainBinding.search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.d(TAG, "onQueryTextSubmit: " + query);
                // TODO do smtg with submit logic
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                setListViewVisible();
                mRoomAdapter.getFilter().filter(newText);

                if (newText.isEmpty()) {
                    Log.d(TAG, "onQueryTextChange: Clear all text pressed. Clearing ListView");
                    setListViewInvisible();
                } else {
                    setListViewVisible();
                }
                return false;
            }
        });
    }

    private void setOnItemClickListener() {
        this.listView = (ListView) findViewById(R.id.list_view);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object o = listView.getItemAtPosition(position);
                Log.d(TAG, "onItemClick: Object Selected is: " + o);


                Intent returnIntent = getIntent();
                returnIntent.putExtra("RESULT", new Gson().toJson(o));
                returnIntent.putExtra("REQUEST_CODE", Variables.REQUEST_CODE_SEARCH);
                setResult(Integer.valueOf(Variables.REQUEST_CODE_SEARCH), returnIntent);

                finish();
            }
        });
    }


    private void setListViewInvisible() {
        this.listView.setVisibility(View.INVISIBLE);
    }

    private void setListViewVisible() {
        this.listView.setVisibility(View.VISIBLE);
    }

}