package com.adro.fcul.portugal.indoornavigationsystem.conectivity;


import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by adroa on 2017-07-02.
 */

public class ApiService extends HttpUtils {
    private static final String TAG = "ApiService";

    private String endPoint;
    private RequestParams mRequestParams;

    private String jsonResult;


    public ApiService(String endPoint) {
        this.endPoint = endPoint;
    }

    public void connectToServiceGet(final OnJsonResponseCallBack callback) throws JSONException {
        // called before request is started
        Log.d(TAG, "connectToServiceGet:" +
                "|| URL => " + endPoint +
                " || PARAM => " + mRequestParams);

        get(endPoint, mRequestParams, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
                Log.d(TAG, "Api Util onStart:" +
                        "|| URL => " + endPoint +
                        " || PARAM => " + mRequestParams);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                Log.d(TAG, "onSuccess: JSONObject" + response);

                callback.onJsonResponse(true, response);


            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                Log.d(TAG, "onSuccess: JSONArray: \n");

                callback.onJsonResponse(true, response);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {
                Log.d("Failed: ", "" + statusCode);
                Log.d("Error : ", "" + throwable);
                callback.onJsonResponse(false, response);

            }
        });


    }


    public String connectToServicePost(String body) { //TODO
        return "";
    }

    public RequestParams getRequestParams() {
        return mRequestParams;
    }

    public void setRequestParams(RequestParams requestParams) {
        mRequestParams = requestParams;
    }



}
