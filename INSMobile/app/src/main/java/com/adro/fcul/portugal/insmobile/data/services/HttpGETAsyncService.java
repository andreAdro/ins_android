package com.adro.fcul.portugal.insmobile.data.services;

import android.os.AsyncTask;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.commons.io.IOUtils;
import timber.log.Timber;

public class HttpGETAsyncService extends AsyncTask<String, String, String> {

  private URL url;
  private HttpAsyncCallback httpAsyncCallback;

  public HttpGETAsyncService(URL url, HttpAsyncCallback httpAsyncCallback) {
    this.url = url;
    this.httpAsyncCallback = httpAsyncCallback;
  }

  @Override
  protected String doInBackground(String... strings) {
    HttpURLConnection connection = null;
    try {
      connection = (HttpURLConnection) this.url.openConnection();
      connection.connect();
      int responseCode = connection.getResponseCode();
      if (responseCode > 199 && responseCode < 300) {
        return IOUtils.toString(connection.getInputStream(), "UTF-8");
      }
    } catch (MalformedURLException e) {
      Timber.e(e);
    } catch (IOException e) {
      Timber.e(e);
    } finally {
      if (connection != null) {
        connection.disconnect();
      }
    }
    RuntimeException runtimeException = new RuntimeException("Error obtaining data...");
    Timber.e(runtimeException);
    throw runtimeException;
  }

  @Override
  protected void onPostExecute(String s) {
    this.httpAsyncCallback.callBack(s);
  }
}
