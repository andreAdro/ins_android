package com.adro.fcul.portugal.insmobile.dto.server.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public abstract class WINSEntity<T> implements Serializable {

  private static final long serialVersionUID = 5232221529717121993L;

  @Include
  private UUID identifier;
  private Date createdDate;
  private Date updatedDate;

  public abstract void update(T t);
}
