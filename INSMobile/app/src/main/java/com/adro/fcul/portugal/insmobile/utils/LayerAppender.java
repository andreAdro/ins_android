package com.adro.fcul.portugal.insmobile.utils;

import com.adro.fcul.portugal.insmobile.utils.GeoJson.GeoJsonType;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.style.layers.FillExtrusionLayer;
import com.mapbox.mapboxsdk.style.layers.Layer;

public class LayerAppender {

  private LayerAppender() {

  }

  public static Layer addLayerToMap(GeoJson geoJson, MapboxMap mapboxMap) {
    if (geoJson.getType()
        .equals(GeoJsonType.POLYGON)) {
//      mapboxMap.addSource(new GeoJsonSource(geoJson.getGeoJsonObject(), geoJson.getSourceKey()));
      FillExtrusionLayer extrusionLayer = new FillExtrusionLayer(geoJson.getLayerKey(),
          geoJson.getSourceKey());
//      mapboxMap.addLayer(extrusionLayer);
      return extrusionLayer;
    }
    return null;
  }
}
