package com.adro.fcul.portugal.insmobile.utils;

import com.adro.fcul.portugal.insmobile.dto.server.geo_json.FeatureObjectDTO;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.List;

public final class GeoJsonPropertiesParserUtil {

  private GeoJsonPropertiesParserUtil() {
  }

  public static List<FeatureObjectDTO> objectToFeatureObjectDTOS(Object object) {
    String jsonString = new Gson().toJson(object);
    Type listType = new TypeToken<List<FeatureObjectDTO>>() {
    }.getType();
    return new Gson().fromJson(jsonString, listType);
  }
}
