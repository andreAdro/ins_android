package com.adro.fcul.portugal.insmobile.activity.search_activity.action;

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.SimpleOnItemTouchListener;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.widget.SearchView;
import com.adro.fcul.portugal.insmobile.data.services.geo_json.point_of_interest.GeoJsonPointOfInterestGETAsyncService;
import com.adro.fcul.portugal.insmobile.dto.app.Searchable;
import com.adro.fcul.portugal.insmobile.dto.server.json.PointOfInterestDTO;
import com.adro.fcul.portugal.insmobile.utils.NavigationUtil;
import java.util.ArrayList;
import java.util.List;

// Handles all the behavior on RecyclerView
public class SearchableItemClickListener extends SimpleOnItemTouchListener implements
    OnRecyclerViewClickListener {

  private AppCompatActivity callerActivity;
  private Context context;
  private GestureDetectorCompat gestureDetector;
  private SearchView searchView;
  List<Searchable> searchables;

  public SearchableItemClickListener(Context context, RecyclerView recyclerView,
      SearchView searchView,
      List<Searchable> searchables, AppCompatActivity caller) {
    this.callerActivity = caller;
    this.context = context;
    this.searchView = searchView;
    this.searchables = searchables;
    SearchableGestureDetector simpleGestureDetector = new SearchableGestureDetector(this,
        recyclerView);
    this.gestureDetector = new GestureDetectorCompat(context, simpleGestureDetector);
  }

  @Override
  public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
    this.searchView.clearFocus();
    return gestureDetector != null && gestureDetector.onTouchEvent(e);
  }

  @Override
  public void onItemClick(View view, int position) {
    PointOfInterestDTO pointOfInterest = fromSearchableToPointOfInterest(
        getSearchableByHintAndPosition(
            String.valueOf(this.searchView.getQuery()), position));
    new GeoJsonPointOfInterestGETAsyncService()
        .GETById(pointOfInterest.getIdentifier()
                .toString(), pointDTO -> {
              NavigationUtil.setGoal(pointDTO, this.context);
              this.callerActivity.finish();
            }
        );
  }

  private PointOfInterestDTO fromSearchableToPointOfInterest(Searchable searchable) {
    return searchable.getPointOfInterest();
  }

  // not the best solution, TODO needs refactor of solution implementation
  private Searchable getSearchableByHintAndPosition(String hint, int position) {
    List<Searchable> newSearchables = new ArrayList<>();
    for (Searchable searchable : this.searchables) {
      if (searchable.getName()
          .contains(hint)) {
        newSearchables.add(searchable);
      }
    }
    // doest not check size to ensure it throws indexOutOfBonds || refactor solution in case it does
    return newSearchables.get(position);
  }

  // Inner class to handle all gestures performed on RecyclerView
  class SearchableGestureDetector extends SimpleOnGestureListener {

    OnRecyclerViewClickListener clickListener;
    RecyclerView recyclerView;

    SearchableGestureDetector(OnRecyclerViewClickListener clickListener,
        RecyclerView recyclerView) {
      this.clickListener = clickListener;
      this.recyclerView = recyclerView;
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
      View childView = recyclerView.findChildViewUnder(e.getX(), e.getY());
      this.clickListener.onItemClick(childView, recyclerView.getChildAdapterPosition(childView));
      return true;
    }
  }
}
