package com.adro.fcul.portugal.insmobile.dto.server.geo_json;

import com.adro.fcul.portugal.insmobile.dto.server.geo_json._enum.FeatureType;
import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FeaturesCollectionDTO {

  private FeatureType type;
  private List<FeatureObjectDTO> features;

}
