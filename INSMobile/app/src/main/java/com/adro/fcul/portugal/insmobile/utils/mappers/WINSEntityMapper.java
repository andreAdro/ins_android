package com.adro.fcul.portugal.insmobile.utils.mappers;

import com.adro.fcul.portugal.insmobile.dto.server.entity.WINSEntity;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json.FeatureObjectDTO;

public abstract class WINSEntityMapper {

  protected void appendWinsEntityData(WINSEntity entity, FeatureObjectDTO featureObjectDTO) {
    entity.setIdentifier(featureObjectDTO.getId());
  }

}
