package com.adro.fcul.portugal.insmobile.data;

import android.os.AsyncTask;
import android.util.Log;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

public class JSONAsyncTask extends AsyncTask<String, Void, String> {

  private static final String TAG = "JSONAsyncTask";
  private final AsyncReceiver receiver;
  private final String callType;
  private String responseBody;
  private String requestBody;

  JSONAsyncTask(AsyncReceiver receiver, String callType) {
    this.receiver = receiver;
    this.callType = checkCallType(callType);
  }

  JSONAsyncTask(AsyncReceiver receiver, String callType, String requestBody) {
    this.receiver = receiver;
    this.callType = checkCallType(callType);
    this.requestBody = requestBody;
  }

  private String checkCallType(String callType) {
    if (!callType.equals("GET")) {
      Log.d(TAG, "checkCallType: " + callType);
    }
    return callType;
  }

  @Override
  protected String doInBackground(String... strings) {
    HttpURLConnection connection = null;
    try {
      URL url = new URL(strings[0]);

      connection = (HttpURLConnection) url.openConnection();
      connection.setRequestMethod(callType);
      if (StringUtils.isNotBlank(this.requestBody)) {
        setConnectionBody(connection, this.requestBody);
      }
      connection.connect();
      int response = connection.getResponseCode();
      if (response > 199 && response < 300) {
        this.responseBody = IOUtils.toString(connection.getInputStream(), "UTF-8");
      } else {
        Log.d(TAG, "doInBackground: The responde code was " + response);
      }
    } catch (MalformedURLException e) {
      Log.e(TAG, "doInBackground: Invalid URL" + e.getMessage());
    } catch (IOException e) {
      Log.e(TAG, "doInBackground: IO Exception reading data:" + e.getMessage());
    } catch (SecurityException e) {
      Log.e(TAG, "doInBackground: Security Exception. Needs permission? " + e.getMessage());
    } finally {
      if (connection != null) {
        connection.disconnect();
      }
    }
    return null;
  }

  private void setConnectionBody(HttpURLConnection connection, String requestBody)
      throws IOException {
    connection.setRequestProperty("Content-Type", "application/json");
    BufferedWriter writer = new BufferedWriter(
        new OutputStreamWriter(connection.getOutputStream()));
    writer.write(requestBody);
    writer.flush();
  }

  @Override
  protected void onPostExecute(String s) {
    // Commented since super.onPostExecute is an empty method
    // super.onPostExecute(s);
    Log.d(TAG, "onPostExecute: starts");
    receiver.receiveCallBack(responseBody);
  }
}
