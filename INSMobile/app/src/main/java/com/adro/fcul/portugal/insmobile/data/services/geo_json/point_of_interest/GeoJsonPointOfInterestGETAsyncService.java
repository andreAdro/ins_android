package com.adro.fcul.portugal.insmobile.data.services.geo_json.point_of_interest;

import com.adro.fcul.portugal.insmobile.data.DataAsyncReceiver;
import com.adro.fcul.portugal.insmobile.data.services.HttpGETAsyncService;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json.FeatureObjectDTO;
import com.google.gson.Gson;
import java.net.MalformedURLException;
import java.net.URL;
import timber.log.Timber;

public class GeoJsonPointOfInterestGETAsyncService extends GeoJsonPointOfInterestAsyncService {

  private DataAsyncReceiver<FeatureObjectDTO> dataAsyncReceiver;

  public void GETById(String id, DataAsyncReceiver<FeatureObjectDTO> pointDTODataAsyncReceiver) {
    this.dataAsyncReceiver = pointDTODataAsyncReceiver;
    URL url = buildUrlWithId(id);
    HttpGETAsyncService httpGetAsyncService = new HttpGETAsyncService(url, this);
    httpGetAsyncService.execute();
  }

  private URL buildUrlWithId(String id) {
    try {
      return new URL(String.format("%s/%s", this.url.toExternalForm(), id));
    } catch (MalformedURLException e) {
      Timber.e(e);
      throw new RuntimeException("Unable to create a valid URL");
    }
  }

  @Override
  public void callBack(String response) {
    this.dataAsyncReceiver.receiveData(jsonToPoint(response));
  }

  private FeatureObjectDTO jsonToPoint(String json) {
    return new Gson().fromJson(json, FeatureObjectDTO.class);
  }

}
