package com.adro.fcul.portugal.insmobile.dto.server.entity.physical;

import com.adro.fcul.portugal.insmobile.dto.server.entity.geometry.Polygon;
import java.util.Set;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
public class Building extends Polygon {

  private static final long serialVersionUID = -8358420541827886217L;

  private String name;
  private Set<Floor> floors;
  private Set<PointOfInterest> pointsOfInterest;

  public void update(Building building) {
    super.update(building);
    this.name = building.getName();
    this.floors = building.getFloors();
    this.pointsOfInterest = building.getPointsOfInterest();
  }
}
