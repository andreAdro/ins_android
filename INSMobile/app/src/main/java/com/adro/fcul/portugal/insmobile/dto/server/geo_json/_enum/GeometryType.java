package com.adro.fcul.portugal.insmobile.dto.server.geo_json._enum;

import com.google.gson.annotations.SerializedName;

public enum GeometryType {

  @SerializedName("Point")
  POINT,
  @SerializedName("MultiPoint")
  MULTI_POINT,
  @SerializedName("LineString")
  LINE_STRING,
  @SerializedName("MultiLineString")
  MULTI_LINE_STRING,
  @SerializedName("Polygon")
  POLYGON,
  @SerializedName("MultiPolygon")
  MULTI_POLYGON,
  @SerializedName("GeometryCollection")
  GEOMETRY_COLLECTION
}
