package com.adro.fcul.portugal.insmobile.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.adro.fcul.portugal.insmobile.common.Constants;
import com.adro.fcul.portugal.insmobile.dto.server.PointOfInterest;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json.FeatureObjectDTO;
import com.google.gson.Gson;

public class NavigationUtil {

  private NavigationUtil() {
  }

  public static PointOfInterest getCurrentPosition(Context context) {
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(
        context);
    return new Gson().fromJson(
        sharedPreferences.getString(Constants.SharedPreferences.CURRENT_POSITION, ""),
        PointOfInterest.class);
  }


  public static void setCurrentPosition(PointOfInterest start, Context context) {
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(
        context);
    sharedPreferences.edit()
        .putString(Constants.SharedPreferences.CURRENT_POSITION, new Gson().toJson(start))
        .apply();
  }

  public static FeatureObjectDTO getStart(Context context) {
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(
        context);
    return new Gson().fromJson(
        sharedPreferences.getString(Constants.SharedPreferences.START, ""),
        FeatureObjectDTO.class);
  }


  public static void setStart(FeatureObjectDTO start, Context context) {
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(
        context);
    sharedPreferences.edit()
        .putString(Constants.SharedPreferences.START, new Gson().toJson(start))
        .apply();
  }


  public static FeatureObjectDTO getGoal(Context context) {
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(
        context);
    return new Gson().fromJson(
        sharedPreferences.getString(Constants.SharedPreferences.GOAL, ""),
        FeatureObjectDTO.class);
  }

  public static void setGoal(FeatureObjectDTO goal, Context context) {
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(
        context);
    sharedPreferences.edit()
        .putString(Constants.SharedPreferences.GOAL, new Gson().toJson(goal))
        .apply();
  }
}
