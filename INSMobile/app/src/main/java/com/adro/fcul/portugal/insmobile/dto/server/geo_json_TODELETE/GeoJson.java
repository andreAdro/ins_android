package com.adro.fcul.portugal.insmobile.dto.server.geo_json_TODELETE;

import java.util.HashMap;
import lombok.Data;

@Data
public class GeoJson {

  protected String type;
  protected HashMap<String, String> properties;

}
