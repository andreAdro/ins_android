package com.adro.fcul.portugal.insmobile.utils.mappers.geo_json.geometry;

import com.adro.fcul.portugal.insmobile.dto.server.entity.geometry.Point;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json.PointDTO;
import com.adro.fcul.portugal.insmobile.utils.mappers.WINSEntityMapper;

public class PointMapper extends WINSEntityMapper {

  public PointMapper() {
  }

  protected Point geoJsonCoordinateToPoint(PointDTO pointCoordinates) {
    Point point = new Point();
    point.setAltitude(pointCoordinates.getAltitude());
    point.setLongitude(pointCoordinates.getLongitude());
    point.setLatitude(pointCoordinates.getLatitude());
    return point;
  }

  protected Double[] pointToGeoJsonCoordinates(Point point) {
    return point.getAltitude() == null
        ? new Double[]{point.getLongitude(), point.getLatitude()}
        : new Double[]{point.getLongitude(), point.getLatitude(), point.getAltitude()};
  }

}
