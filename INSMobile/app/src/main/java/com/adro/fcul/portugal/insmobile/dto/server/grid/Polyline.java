package com.adro.fcul.portugal.insmobile.dto.server.grid;


import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Polyline {

  @SerializedName("identifier")
  private Integer id;

  @SerializedName("weight")
  private Double weight;

  @SerializedName("start")
  private Point start;

  @SerializedName("end")
  private Point end;

}
