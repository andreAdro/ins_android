package com.adro.fcul.portugal.insmobile.utils;

import static com.adro.fcul.portugal.insmobile.utils.GeoJson.GeoJsonLayerProperties.LAYER_ID;
import static com.adro.fcul.portugal.insmobile.utils.GeoJson.GeoJsonLayerProperties.SOURCE_ID;

import java.util.HashMap;
import java.util.Locale;

public abstract class GeoJson {

  protected final HashMap<String, String> properties = new HashMap<>();

  public abstract GeoJsonType getType();

  public abstract String getGeoJsonObject();

  public String getLayerKey() {
    return this.properties.get(LAYER_ID);
  }

  public String getSourceKey() {
    return this.properties.get(SOURCE_ID);
  }

  public enum GeoJsonType {
    POINT, MULTI_POINT, LINE_STRING, MULTILINE_STRING, POLYGON, MULTI_POLYGON, GEOMETRY_COLLECTION
  }

  class GeoJsonLayerProperties {

    public static final String SOURCE_ID = "sourceId";
    public static final String LAYER_ID = "layerId";
    public static final String COLOR = "color";

    private class PolygonProperties {

      public static final String EXTRUSION_BASE_HEIGHT = "base_height";
      public static final String EXTRUSION_HEIGHT = "height";
    }
  }

  protected String buildLayerId(int id, String entityName) {
    return String.format(Locale.getDefault(), "%d%sLayer", id, entityName);
  }

  protected String buildSourceId(int id, String entityName) {
    return String.format(Locale.getDefault(), "%d%sSource", id, entityName);
  }

}
