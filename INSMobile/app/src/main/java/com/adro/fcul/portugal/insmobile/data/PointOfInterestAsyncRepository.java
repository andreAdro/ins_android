package com.adro.fcul.portugal.insmobile.data;

import static com.adro.fcul.portugal.insmobile.common.Constants.EndpointConstants.POINT_OF_INTEREST_CONTROL_POINTS;
import static com.adro.fcul.portugal.insmobile.common.Constants.HOST;
import static com.adro.fcul.portugal.insmobile.common.Constants.PORT;
import static com.adro.fcul.portugal.insmobile.common.Constants.PROTOCOL;

import android.content.Context;
import com.adro.fcul.portugal.insmobile.dto.server.PointOfInterest;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.List;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class PointOfInterestAsyncRepository extends AsyncRepository<PointOfInterest> {

  public PointOfInterestAsyncRepository(Context context) {
    //  https://stackoverflow.com/questions/32444863/google-gson-linkedtreemap-class-cast-to-myclass/32494079#32494079
    Type type = new TypeToken<PointOfInterest>() {
    }.getType();
    Type typeCollection = new TypeToken<List<PointOfInterest>>() {
    }.getType();
    String url =
        PROTOCOL + "://" + HOST + ":" + PORT + POINT_OF_INTEREST_CONTROL_POINTS;
    super.start(context, PointOfInterestAsyncRepository.class.getSimpleName(), type, typeCollection,
        url,
        true);
  }

}
