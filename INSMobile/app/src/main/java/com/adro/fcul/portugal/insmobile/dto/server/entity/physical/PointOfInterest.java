package com.adro.fcul.portugal.insmobile.dto.server.entity.physical;

import com.adro.fcul.portugal.insmobile.dto.server.entity.geometry.Point;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
public class PointOfInterest extends Point {

  private static final long serialVersionUID = 2283369597697708645L;

  private String name;

  public void update(PointOfInterest point) {
    super.update(point);
    this.name = point.getName();
  }
}
