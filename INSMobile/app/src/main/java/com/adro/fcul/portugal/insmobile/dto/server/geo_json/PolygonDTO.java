package com.adro.fcul.portugal.insmobile.dto.server.geo_json;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PolygonDTO extends FeatureObjectDTO {

}
