package com.adro.fcul.portugal.insmobile.common;


public class Constants {

  public static final String PROTOCOL = "http";
  public static final String HOST = "192.168.1.8";// "10.0.2.2";
  public static final int PORT = 8080;
  private static final String API = "wifi-indoor-navigation-system/api/v1";
  // LOCALHOST
  public static final String LOCATION = String.format("%s://%s:%s/%s", PROTOCOL, HOST, PORT, API);
// HEROKU
//  public static final String LOCATION = "https://fcul-wins.herokuapp.com/wifi-indoor-navigation-system/api/v1";


  public class MapBoxConstants {

    public static final String STYLE_URL = "mapbox://styles/dotadro/cjjwz8p1p6lsg2sqqv7ha89p9"; // C8 3D-copy
//    public static final String STYLE_URL = "mapbox://styles/dotadro/cjiyg1h0l10ta2smscjvphqfb"; // C8 3D

  }

  public class SharedPreferences {

    public static final String CURRENT_POSITION = "CURRENT_POSITION";
    public static final String START = "START";
    public static final String GOAL = "GOAL";


  }

  public class ActivityIntent {

    public static final String CLICKED_MARKER_ID = "clickedMarkerId";
  }

  public class ActivityForResult {

    public static final int SEARCH_ACTIVITY = 1;
    public static final int CONTROL_POINTS_MAP_ACTIVITY = 2;
  }

  public class EndpointConstants {


    public static final String POINT_OF_INTEREST_CONTROL_POINTS = API + "/pointOfInterest/control";
    public static final String ACCESS_POINT_FREQUENCY = API + "/accessPointFrequency";
    public static final String ROUTE_POLYLINES = API + "/route/polylines";
    public static final String GEO_JSON_ROOMS = API + "/geoJson/rooms";
    public static final String GEO_JSON_ROUTES = API + "/geoJson/routes";

  }

  public class EndpointParameters {

    public static final String ACCESS_POINT_FREQUENCY_POINT_ID = "pointId";

  }
}