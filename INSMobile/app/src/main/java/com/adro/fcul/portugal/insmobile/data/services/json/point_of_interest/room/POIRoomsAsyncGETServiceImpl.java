package com.adro.fcul.portugal.insmobile.data.services.json.point_of_interest.room;

import com.adro.fcul.portugal.insmobile.data.DataCollectionAsyncReceiver;
import com.adro.fcul.portugal.insmobile.data.services.HttpGETAsyncService;
import com.adro.fcul.portugal.insmobile.dto.server.json.RoomDTO;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class POIRoomsAsyncGETServiceImpl extends POIRoomAsyncService {

  private DataCollectionAsyncReceiver<RoomDTO> dataCollectionAsyncReceiver;

  public POIRoomsAsyncGETServiceImpl(String pointOfInterestId) {
    super(pointOfInterestId);
  }

  public void GET(DataCollectionAsyncReceiver<RoomDTO> dataCollectionAsyncReceiver) {
    this.dataCollectionAsyncReceiver = dataCollectionAsyncReceiver;
    HttpGETAsyncService httpGETAsyncService =
        new HttpGETAsyncService(url, this);
    httpGETAsyncService.execute();
  }

  @Override
  public void callBack(String response) {
    this.dataCollectionAsyncReceiver.receiveData(jsonToRooms(response));
  }

  private List<RoomDTO> jsonToRooms(String json) {
    Type listType = new TypeToken<ArrayList<RoomDTO>>() {
    }.getType();
    return new Gson().fromJson(json, listType);
  }
}
