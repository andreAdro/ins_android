package com.adro.fcul.portugal.insmobile.data.services;

import android.os.AsyncTask;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.commons.io.IOUtils;
import timber.log.Timber;

public class HttpPOSTAsyncService extends AsyncTask<String, String, String> {

  private final URL url;
  private final HttpAsyncCallback httpAsyncCallback;
  private final String json;

  public HttpPOSTAsyncService(URL url, HttpAsyncCallback httpAsyncCallback, String json) {
    this.url = url;
    this.httpAsyncCallback = httpAsyncCallback;
    this.json = json;
  }


  @Override
  protected String doInBackground(String... strings) {
    HttpURLConnection connection = null;
    OutputStream out;
    try {
      connection = (HttpURLConnection) this.url.openConnection();
      connection.setRequestProperty("Content-Type", "application/json");
      connection.setRequestProperty("Accept", "*/*");
      connection.setDoOutput(true);

//      out = new BufferedOutputStream(connection.getOutputStream());

      BufferedWriter writer = new BufferedWriter(
          new OutputStreamWriter(connection.getOutputStream()));
      writer.write(json);
      writer.close();

      connection.connect();
      int responseCode = connection.getResponseCode();
      if (responseCode > 199 && responseCode < 300) {
        return IOUtils.toString(connection.getInputStream(), "UTF-8");
      }
    } catch (MalformedURLException e) {
      Timber.e(e);
    } catch (IOException e) {
      Timber.e(e);
    } finally {
      if (connection != null) {
        connection.disconnect();
      }
    }
    RuntimeException runtimeException = new RuntimeException("Error obtaining data...");
    Timber.e(runtimeException);
    throw runtimeException;
  }
}
