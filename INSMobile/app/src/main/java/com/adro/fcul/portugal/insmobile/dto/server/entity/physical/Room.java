package com.adro.fcul.portugal.insmobile.dto.server.entity.physical;

import com.adro.fcul.portugal.insmobile.dto.server.entity.geometry.Polygon;
import java.util.Set;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
public class Room extends Polygon {

  private static final long serialVersionUID = -8232018833197963092L;

  private String name;
  private Floor floor;
  private Set<PointOfInterest> pointsOfInterest;

  public void update(Room room) {
    super.update(room);
    this.name = room.getName();
    this.floor = room.getFloor();
    this.pointsOfInterest = room.getPointsOfInterest();
  }
}
