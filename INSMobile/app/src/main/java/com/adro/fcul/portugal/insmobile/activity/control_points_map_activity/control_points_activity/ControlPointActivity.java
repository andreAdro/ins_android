package com.adro.fcul.portugal.insmobile.activity.control_points_map_activity.control_points_activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;
import com.adro.fcul.portugal.insmobile.R;
import com.adro.fcul.portugal.insmobile.common.Constants.ActivityIntent;
import com.adro.fcul.portugal.insmobile.data.services.geo_json.navigation.control_point.frequencies.JsonFrequenciesGETAsyncService;
import com.adro.fcul.portugal.insmobile.data.services.geo_json.navigation.control_point.frequencies.JsonFrequencyPOSTAsyncService;
import com.adro.fcul.portugal.insmobile.dto.server.json.FrequencyDTO;
import com.adro.fcul.portugal.insmobile.utils.WifiUtil;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;

public class ControlPointActivity extends AppCompatActivity implements OnCheckedChangeListener {

  private static final String TAG = "ControlPointActivity";

  private List<FrequencyDTO> accessPointFrequencies = new ArrayList<>();
  private List<FrequencyDTO> pendingAccessPointFrequencies = new ArrayList<>();
  private RecyclerView recyclerView;
  private ControlPointFrequencyRecyclerViewAdapter adapter;
  private Switch parseButton;
  private boolean isParsingRSSI;
  private Handler rssiTimeHandler;
  private Menu menu;
  private String controlPointId;
  private int pendingAccessPointFrequenciesCounter;

  private BroadcastReceiver wifiBroadcastReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      WifiManager wifiManager = (WifiManager) context.getApplicationContext()
          .getSystemService(
              Context.WIFI_SERVICE);
      pushWifiFrequenciesToAdapter();
      wifiManager.startScan();
    }
  };

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_control_point);
    activateToolbar(true);
    createSwitchButton();
    populateData();
    addRSSIHandlerListener();
  }

  private void populateData() {
    this.controlPointId = getIntent().getStringExtra(ActivityIntent.CLICKED_MARKER_ID)
        .replaceAll("\"", "");

    new JsonFrequenciesGETAsyncService(controlPointId).GET(frequencies -> {
      this.accessPointFrequencies = frequencies;
      createRecyclerView();
    });
  }

  private void createSwitchButton() {
    this.parseButton = findViewById(R.id.button_rssi_calibration);
    this.parseButton.setChecked(false);
    this.parseButton.setOnCheckedChangeListener(this);
  }

  @Override
  public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
    this.isParsingRSSI = isChecked;
    if (isChecked) {
      setMenuCancelItem(true);
    } else {
      this.unregisterReceiver(wifiBroadcastReceiver);
    }
  }

  private void createRecyclerView() {
    this.recyclerView = findViewById(R.id.access_point_frequency_recycler_view);
    recyclerView.setLayoutManager(new LinearLayoutManager(this));
    adapter = new ControlPointFrequencyRecyclerViewAdapter(this.accessPointFrequencies);
    recyclerView.setAdapter(adapter);
  }

  public void addRSSIHandlerListener() {
    rssiTimeHandler = new Handler();
    int delay = 2000; // milliseconds -> 1 second
    rssiTimeHandler.postDelayed(new Runnable() {
      @Override
      public void run() {
        if (isParsingRSSI) {
          startScanning();
        }
        rssiTimeHandler.postDelayed(this, delay);
      }
    }, delay);
  }

  private void startScanning() {
    this.registerReceiver(
        wifiBroadcastReceiver,
        new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)
    );
    WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(
        Context.WIFI_SERVICE);
    wifiManager.startScan();
  }

  private void pushWifiFrequenciesToAdapter() {
    List<FrequencyDTO> frequencies = getFrequenciesFromWifiManagement();
    for (FrequencyDTO frequency : frequencies) {
      pushNewAccessPointFrequencyToAdapter(frequency);
      this.pendingAccessPointFrequencies.add(frequency);
    }
    this.recyclerView.scrollToPosition(0);
  }


  private List<FrequencyDTO> getFrequenciesFromWifiManagement() {
    List<FrequencyDTO> newFrequencies = new ArrayList<>();
    WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(
        Context.WIFI_SERVICE);
    List<ScanResult> wifiList = wifiManager.getScanResults();
    wifiManager.startScan();
    for (ScanResult scanResult : wifiList) {
      if (scanResult.level > -75) {
        FrequencyDTO accessPointFrequency = WifiUtil.getFrequencyFromScanResult(
            scanResult);
        newFrequencies.add(accessPointFrequency);
      }
    }
    return newFrequencies;
  }

  private void pushNewAccessPointFrequencyToAdapter(FrequencyDTO accessPointFrequency) {
    adapter.addItem(accessPointFrequency);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        this.rssiTimeHandler.removeCallbacks(null);
        resetActivityState();
        onBackPressed();
        finish();
        return true;
      case R.id.app_bar_save:
        resetActivityState();
        saveNewData();
        finish();
        break;
      case R.id.app_bar_cancel:
        resetActivityState();
        resetActivityData();
        break;
    }
    return super.onOptionsItemSelected(item);
  }

  private void saveNewData() {
    this.pendingAccessPointFrequenciesCounter = this.pendingAccessPointFrequencies.size();
    for (FrequencyDTO frequencyDTO : this.pendingAccessPointFrequencies) {
      new JsonFrequencyPOSTAsyncService(this.controlPointId, new Gson().toJson(frequencyDTO)).POST(
          uuid -> {
            this.pendingAccessPointFrequenciesCounter--;
            if (this.pendingAccessPointFrequenciesCounter == 0) {
              finish();
            }
          });
    }
    if (this.pendingAccessPointFrequenciesCounter == 0) {
      finish();
    }
  }

  public void resetActivityData() {
    this.adapter.resetWithNewData(accessPointFrequencies);
    this.pendingAccessPointFrequencies.clear();
  }

  private void resetActivityState() {
    setMenuCancelItem(false);
    setButtonParseChecked(false);
  }

  private void activateToolbar(boolean enableHome) {
    Log.d(TAG, "activateToolbar: start");
    ActionBar actionBar = getSupportActionBar();
    if (actionBar == null) {
      Toolbar toolbar = findViewById(R.id.toolbar);
      if (toolbar != null) {
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
      }
    }
    if (actionBar != null) {
      actionBar.setDisplayHomeAsUpEnabled(enableHome);
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_control_point, menu);
    return true;
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    this.menu = menu;
    // hides cross sign from menu
    setMenuCancelItem(false);

    return super.onPrepareOptionsMenu(menu);
  }

  private void setMenuCancelItem(boolean isVisible) {
    this.menu.getItem(0)
        .setVisible(isVisible);
  }

  private void setButtonParseChecked(boolean isChecked) {
    this.parseButton.setChecked(isChecked);
  }

}
