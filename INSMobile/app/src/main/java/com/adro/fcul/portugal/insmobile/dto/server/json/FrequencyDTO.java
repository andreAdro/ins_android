package com.adro.fcul.portugal.insmobile.dto.server.json;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class FrequencyDTO extends WINSDTO {

  private String bssid;
  private String ssid;
  private Double rssi;

}
