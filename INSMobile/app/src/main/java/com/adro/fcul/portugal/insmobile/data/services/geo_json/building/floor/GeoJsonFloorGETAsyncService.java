package com.adro.fcul.portugal.insmobile.data.services.geo_json.building.floor;

import com.adro.fcul.portugal.insmobile.data.DataAsyncReceiver;
import com.adro.fcul.portugal.insmobile.data.services.HttpGETAsyncService;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json.FeatureObjectDTO;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class GeoJsonFloorGETAsyncService extends GeoJsonFloorAsyncService {

  private DataAsyncReceiver<List<FeatureObjectDTO>> dataAsyncReceiver;

  public GeoJsonFloorGETAsyncService(String buildingId) {
    super(buildingId);
  }

  public void GET(DataAsyncReceiver<List<FeatureObjectDTO>> dataAsyncReceiver) {
    this.dataAsyncReceiver = dataAsyncReceiver;
    HttpGETAsyncService httpGETAsyncService = new HttpGETAsyncService(url, this);
    httpGETAsyncService.execute();
  }

  @Override
  public void callBack(String response) {
    this.dataAsyncReceiver.receiveData(jsonToFeatureObject(response));
  }

  private List<FeatureObjectDTO> jsonToFeatureObject(String json) {
    Type listType = new TypeToken<ArrayList<FeatureObjectDTO>>() {
    }.getType();
    return new Gson().fromJson(json, listType);
  }
}
