package com.adro.fcul.portugal.insmobile.dto.server;


import com.adro.fcul.portugal.insmobile.dto.server.grid.Polyline;
import com.google.gson.annotations.SerializedName;
import java.util.Set;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Route {

  @SerializedName("identifier")
  private long id;

  @SerializedName("name")
  private String name;

  @SerializedName("polylines")
  private Set<Polyline> polylines;
}
