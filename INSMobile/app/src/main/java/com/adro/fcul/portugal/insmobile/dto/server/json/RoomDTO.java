package com.adro.fcul.portugal.insmobile.dto.server.json;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class RoomDTO extends WINSDTO {

  private static final long serialVersionUID = 2966505167018277644L;
  private String name;

}
