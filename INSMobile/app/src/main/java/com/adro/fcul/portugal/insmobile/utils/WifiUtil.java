package com.adro.fcul.portugal.insmobile.utils;

import android.net.wifi.ScanResult;
import com.adro.fcul.portugal.insmobile.dto.server.json.FrequencyDTO;

public class WifiUtil {

  private WifiUtil() {
  }

  public static FrequencyDTO getFrequencyFromScanResult(ScanResult scanResult) {
    FrequencyDTO frequency = new FrequencyDTO();
    frequency.setBssid(scanResult.BSSID);
    frequency.setSsid(scanResult.SSID);
    frequency.setRssi((double) scanResult.level);
//    frequency.setFrequency(scanResult.frequency);
    return frequency;
  }

}
