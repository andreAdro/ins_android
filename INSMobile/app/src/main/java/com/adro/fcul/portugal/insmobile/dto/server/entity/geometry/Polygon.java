package com.adro.fcul.portugal.insmobile.dto.server.entity.geometry;

import com.adro.fcul.portugal.insmobile.dto.server.entity.WINSEntity;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json._enum.GeometryType;
import java.util.LinkedList;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
public class Polygon extends WINSEntity<Polygon> {

  private static final long serialVersionUID = 9127720855567470795L;

  private GeometryType geometryType;
  private List<Polyline> polylines = new LinkedList<>();

  @Override
  public void update(Polygon polygon) {
    this.geometryType = polygon.getGeometryType();
    this.polylines = polygon.getPolylines();
  }
}
