package com.adro.fcul.portugal.insmobile.data.services.json.point_of_interest;

import com.adro.fcul.portugal.insmobile.data.DataAsyncReceiver;
import com.adro.fcul.portugal.insmobile.data.services.HttpGETAsyncService;
import com.adro.fcul.portugal.insmobile.dto.server.json.PointOfInterestDTO;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import timber.log.Timber;

public class PointOfInterestAsyncGETService extends PointOfInterestAsyncService {

  private DataAsyncReceiver<PointOfInterestDTO> dataAsyncReceiver;

  public PointOfInterestAsyncGETService() {
    super();
  }

  public void GET(DataAsyncReceiver<PointOfInterestDTO> dataAsyncReceiver, String id) {
    this.dataAsyncReceiver = dataAsyncReceiver;
    try {
      URL newUrl = new URL(url.toExternalForm() + "/" + id);
      HttpGETAsyncService httpGetAsyncService =
          new HttpGETAsyncService(newUrl, this);
      httpGetAsyncService.execute();
    } catch (MalformedURLException e) {
      Timber.e(e);
      throw new RuntimeException("Unable to create a valid URL");
    }
  }

  @Override
  public void callBack(String response) {
    this.dataAsyncReceiver.receiveData(jsonToPointOfInterest(response));
  }

  public PointOfInterestDTO jsonToPointOfInterest(String json) {
    Type type = new TypeToken<PointOfInterestDTO>() {
    }.getType();
    return new Gson().fromJson(json, type);
  }
}
