package com.adro.fcul.portugal.insmobile.dto.server;


import com.adro.fcul.portugal.insmobile.dto.server.grid.Point;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PointOfInterest {

  @SerializedName("identifier")
  private Integer id;

  @SerializedName("name")
  private String name;

  @SerializedName("type")
  private PointTypeEnum type;

  @SerializedName("point")
  private Point point;
}
