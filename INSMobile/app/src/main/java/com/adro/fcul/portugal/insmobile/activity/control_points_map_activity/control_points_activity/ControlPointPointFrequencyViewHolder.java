package com.adro.fcul.portugal.insmobile.activity.control_points_map_activity.control_points_activity;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.adro.fcul.portugal.insmobile.R;
import lombok.Getter;

@Getter
class ControlPointPointFrequencyViewHolder extends RecyclerView.ViewHolder {

  private static final String TAG = "AccessPointFrequencyVie";

  private final TextView bssidLabel;
  private final TextView bssidValue;
  private final TextView rssiLabel;
  private final TextView rssiValue;

  ControlPointPointFrequencyViewHolder(View view) {
    super(view);
    this.bssidLabel = view.findViewById(R.id.bssid_label);
    this.bssidValue = view.findViewById(R.id.bssid_value);
    this.rssiLabel = view.findViewById(R.id.rssi_label);
    this.rssiValue = view.findViewById(R.id.rssi_value);
  }

}
