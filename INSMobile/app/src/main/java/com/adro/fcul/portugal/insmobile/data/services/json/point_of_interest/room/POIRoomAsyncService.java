package com.adro.fcul.portugal.insmobile.data.services.json.point_of_interest.room;

import com.adro.fcul.portugal.insmobile.common.Constants;
import com.adro.fcul.portugal.insmobile.data.services.HttpAsyncCallback;
import java.net.MalformedURLException;
import java.net.URL;
import timber.log.Timber;

public abstract class POIRoomAsyncService implements HttpAsyncCallback {

  protected URL url;

  public POIRoomAsyncService(String pointOfInterestId) {
    try {
      String url = String.format("%s/%s/%s/%s", Constants.LOCATION, "points-of-interest",
          pointOfInterestId, "rooms");
      this.url = new URL(url);
    } catch (MalformedURLException e) {
      Timber.e(e);
      throw new RuntimeException("Unable to create a valid URL");
    }
  }

}
