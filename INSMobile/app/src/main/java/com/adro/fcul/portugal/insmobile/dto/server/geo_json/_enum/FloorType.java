package com.adro.fcul.portugal.insmobile.dto.server.geo_json._enum;

import com.google.gson.annotations.SerializedName;

public enum FloorType {

  @SerializedName("Basement")
  BASEMENT,
  @SerializedName("First")
  FIRST,
  @SerializedName("Second")
  SECOND,
  @SerializedName("Third")
  THIRD,
  @SerializedName("Fourth")
  FOURTH,
  @SerializedName("Fifth")
  FIFTH
}
