package com.adro.fcul.portugal.insmobile.dto.server.geo_json_TODELETE;

import com.adro.fcul.portugal.insmobile.dto.server.entity.physical.Room;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
public class GeoJsonRoomFeatureCollection {

  String type = "FeatureCollection";
  List<RoomGeoJson> features;

  @Data
  @EqualsAndHashCode(callSuper = true)
  public class RoomGeoJson extends GeoJson {

    private PolygonGeometry geometry;

    public RoomGeoJson(Room room) {
      this.type = "Feature";
      this.geometry = getGeometry(room);
      this.properties = getProperties(room);

    }

    private PolygonGeometry getGeometry(Room room) {
      PolygonGeometry geometry = new PolygonGeometry();
      geometry.setType("Polygon");
      List<List<Double>> aList = new ArrayList<>();
      geometry.getCoordinates()
          .add(aList);
//      for (Point point : room.getPolygon()
//          .getPoints()) {
//        List<Double> coordinates = new ArrayList<>();
//        double latitude = point.getLongitude();
//        double longitude = point.getLatitude();
//        coordinates.add(latitude);
//        coordinates.add(longitude);
//
//        aList.add(coordinates);
//      }
      return geometry;
    }

    private HashMap<String, String> getProperties(Room room) {
      HashMap<String, String> properties = new HashMap<>();
      properties.put("name", room.getName());
      return properties;
    }

    @Data
    private class PolygonGeometry {

      private String type;
      private List<List<List<Double>>> coordinates = new ArrayList<>();
    }
  }

}
