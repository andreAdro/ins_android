package com.adro.fcul.portugal.insmobile.data.services.geo_json.navigation.control_point.frequencies;

import com.adro.fcul.portugal.insmobile.data.DataAsyncReceiver;
import com.adro.fcul.portugal.insmobile.data.services.HttpPOSTAsyncService;
import com.google.gson.Gson;
import java.util.UUID;

public class JsonFrequencyPOSTAsyncService extends JsonFrequencyAsyncService {

  private final String jsonPayload;
  private DataAsyncReceiver<UUID> dataAsyncReceiver;

  public JsonFrequencyPOSTAsyncService(String controlPointId, String jsonPayload) {
    super(controlPointId);
    this.jsonPayload = jsonPayload;
  }

  public void POST(DataAsyncReceiver<UUID> dataCollectionAsyncReceiver) {
    this.dataAsyncReceiver = dataCollectionAsyncReceiver;
    new HttpPOSTAsyncService(super.url, this, jsonPayload).execute();
  }

  @Override
  public void callBack(String response) {
    this.dataAsyncReceiver.receiveData(jsonToUUID(response));
  }

  private UUID jsonToUUID(String jsonString) {
    return new Gson().fromJson(jsonString, UUID.class);
  }
}
