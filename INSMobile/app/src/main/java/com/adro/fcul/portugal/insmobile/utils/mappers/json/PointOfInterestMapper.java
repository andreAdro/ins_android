package com.adro.fcul.portugal.insmobile.utils.mappers.json;

import com.adro.fcul.portugal.insmobile.dto.server.entity.geometry.Point;
import com.adro.fcul.portugal.insmobile.dto.server.entity.physical.PointOfInterest;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json.FeatureObjectDTO;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json.GeometryDTO;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json._enum.FeatureType;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json._enum.GeometryType;
import com.adro.fcul.portugal.insmobile.utils.mappers.geo_json.geometry.PointMapper;
import java.util.HashMap;
import java.util.Map;

public class PointOfInterestMapper extends PointMapper {

  public PointOfInterestMapper() {
  }

  public PointOfInterest geoJsonToPointOfInterest(FeatureObjectDTO featureObjectDTO) {
    PointOfInterest pointOfInterest = new PointOfInterest();
    appendCoordinate(pointOfInterest, featureObjectDTO);
    appendProperties(pointOfInterest, featureObjectDTO);
    pointOfInterest.setIdentifier(featureObjectDTO.getId());
    return pointOfInterest;
  }

  public FeatureObjectDTO pointOfInterestToGeoJson(PointOfInterest pointOfInterest) {
    FeatureObjectDTO featureObjectDTO = new FeatureObjectDTO();
    extractCoordinate(pointOfInterest, featureObjectDTO);
    extractProperties(pointOfInterest, featureObjectDTO);
    extractOtherData(pointOfInterest, featureObjectDTO);
    return featureObjectDTO;
  }

  private void appendCoordinate(PointOfInterest pointOfInterest,
      FeatureObjectDTO featureObjectDTO) {
    Point point = super.geoJsonCoordinateToPoint(
        featureObjectDTO.getGeometry()
            .getPointCoordinates()
    );
    pointOfInterest.update(point);
  }

  private void extractCoordinate(PointOfInterest pointOfInterest,
      FeatureObjectDTO featureObjectDTO) {
    if (null == featureObjectDTO.getGeometry()) {
      featureObjectDTO.setGeometry(new GeometryDTO());
    }
    Object parsedCoordinates = super.pointToGeoJsonCoordinates(pointOfInterest);
    featureObjectDTO.getGeometry()
        .setCoordinates(parsedCoordinates);
    featureObjectDTO.getGeometry()
        .setType(GeometryType.POINT);
  }

  private void appendProperties(PointOfInterest pointOfInterest,
      FeatureObjectDTO featureObjectDTO) {
    Map<String, Object> properties = featureObjectDTO.getProperties();
    Object doorName = properties.get("name");
    if (doorName instanceof String) {
      pointOfInterest.setName((String) doorName);
    }
  }

  private void extractProperties(PointOfInterest pointOfInterest,
      FeatureObjectDTO featureObjectDTO) {
    Map<String, Object> properties = new HashMap<>();
    properties.put("name", pointOfInterest.getName());
    featureObjectDTO.setProperties(properties);
  }

  private void extractOtherData(PointOfInterest pointOfInterest,
      FeatureObjectDTO featureObjectDTO) {
    featureObjectDTO.setType(FeatureType.FEATURE);
    featureObjectDTO.setId(pointOfInterest.getIdentifier());
  }

//  public abstract PointOfInterestDTO entityToDTO(PointOfInterest entity);

}
