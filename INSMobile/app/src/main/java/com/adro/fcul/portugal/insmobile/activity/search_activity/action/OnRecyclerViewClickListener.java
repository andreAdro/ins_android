package com.adro.fcul.portugal.insmobile.activity.search_activity.action;

import android.view.View;

public interface OnRecyclerViewClickListener {

  void onItemClick(View view, int position);
}
