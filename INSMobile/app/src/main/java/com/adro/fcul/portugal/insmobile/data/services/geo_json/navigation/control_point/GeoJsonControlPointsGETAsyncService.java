package com.adro.fcul.portugal.insmobile.data.services.geo_json.navigation.control_point;

import com.adro.fcul.portugal.insmobile.data.DataCollectionAsyncReceiver;
import com.adro.fcul.portugal.insmobile.data.services.HttpGETAsyncService;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json.FeatureObjectDTO;
import com.adro.fcul.portugal.insmobile.dto.server.json.FrequencyDTO;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import timber.log.Timber;

public class GeoJsonControlPointsGETAsyncService extends GeoJsonControlPointAsyncService {

  private DataCollectionAsyncReceiver<FeatureObjectDTO> dataAsyncReceiver;

  public void GET(DataCollectionAsyncReceiver<FeatureObjectDTO> dataCollectionAsyncReceiver,
      List<FrequencyDTO> frequencies) {
    this.dataAsyncReceiver = dataCollectionAsyncReceiver;
    if (null == frequencies) {
      new HttpGETAsyncService(super.url, this).execute();
    } else {
      new HttpGETAsyncService(buildUrlWithFrequencies(frequencies), this).execute();
    }
  }

  private URL buildUrlWithFrequencies(List<FrequencyDTO> frequencies) {
    try {
      return new URL(
          String.format(
              "%s?frequencies=%s",
              super.url.toExternalForm(),
              URLEncoder.encode(new Gson().toJson(frequencies), "utf-8"))
      );
    } catch (MalformedURLException e) {
      Timber.e(e);
      throw new RuntimeException("Unable to create a valid URL");
    } catch (UnsupportedEncodingException e) {
      Timber.e(e);
      throw new RuntimeException("Unable to create a valid URL");
    }
  }

  @Override
  public void callBack(String response) {
    this.dataAsyncReceiver.receiveData(jsonToPoint(response));
  }

  private List<FeatureObjectDTO> jsonToPoint(String json) {
    Type listType = new TypeToken<ArrayList<FeatureObjectDTO>>() {
    }.getType();
    return new Gson().fromJson(json, listType);
  }
}
