package com.adro.fcul.portugal.insmobile.data;

import android.content.Context;
import com.adro.fcul.portugal.insmobile.common.Constants;
import com.adro.fcul.portugal.insmobile.dto.server.entity.physical.Room;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.List;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class RoomAsyncRepository extends AsyncRepository<Room> {

  public RoomAsyncRepository(Context context) {
    //  https://stackoverflow.com/questions/32444863/google-gson-linkedtreemap-class-cast-to-myclass/32494079#32494079
    Type type = new TypeToken<Room>() {
    }.getType();
    Type typeCollection = new TypeToken<List<Room>>() {
    }.getType();
    String url =
        Constants.PROTOCOL + "://" + Constants.HOST + ":" + Constants.PORT + "/" + "api/rooms";
    super.start(context, RoomAsyncRepository.class.getSimpleName(), type, typeCollection, url,
        true);
  }


}
