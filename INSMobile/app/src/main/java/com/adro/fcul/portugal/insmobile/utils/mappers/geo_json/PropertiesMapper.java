package com.adro.fcul.portugal.insmobile.utils.mappers.geo_json;

import static com.adro.fcul.portugal.insmobile.utils.GeoJsonPropertiesParserUtil.objectToFeatureObjectDTOS;

import com.adro.fcul.portugal.insmobile.dto.server.entity.physical.PointOfInterest;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json.FeatureObjectDTO;
import com.adro.fcul.portugal.insmobile.utils.mappers.json.PointOfInterestMapper;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PropertiesMapper {

  protected PointOfInterestMapper pointOfInterestMapper;

  public PropertiesMapper() {
    this.pointOfInterestMapper = new PointOfInterestMapper();
  }

  public Set<PointOfInterest> propertiesToPointsOfInterest(Object pointsOfInterestArray) {
    LinkedHashSet<PointOfInterest> pointsOfInterest = new LinkedHashSet<>();
    if (pointsOfInterestArray instanceof List) {
      List<FeatureObjectDTO> featureObjectDTOS = objectToFeatureObjectDTOS(pointsOfInterestArray);
      for (FeatureObjectDTO featureObjectDTO : featureObjectDTOS) {
        PointOfInterest pointOfInterest = this.pointOfInterestMapper.geoJsonToPointOfInterest(
            featureObjectDTO);
        pointsOfInterest.add(pointOfInterest);
      }
    }
    return pointsOfInterest;
  }

  public void extractPointsOfInterestToProperties(Set<PointOfInterest> pointsOfInterest,
      Map<String, Object> properties) {
    if (null != pointsOfInterest
        && pointsOfInterest.size() > 0) {
      List<FeatureObjectDTO> geoJsonPointsOfInterest = new ArrayList<>();
      for (PointOfInterest pointOfInterest : pointsOfInterest) {
        FeatureObjectDTO featureObjectDTO = this.pointOfInterestMapper.pointOfInterestToGeoJson(
            pointOfInterest);
        geoJsonPointsOfInterest.add(featureObjectDTO);
      }
      properties.put("pointsOfInterest", geoJsonPointsOfInterest);
    }
  }

}
