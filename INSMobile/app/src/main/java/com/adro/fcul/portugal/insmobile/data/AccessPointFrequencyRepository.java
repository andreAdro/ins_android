package com.adro.fcul.portugal.insmobile.data;

import static com.adro.fcul.portugal.insmobile.common.Constants.EndpointConstants.ACCESS_POINT_FREQUENCY;

import android.content.Context;
import com.adro.fcul.portugal.insmobile.common.Constants;
import com.adro.fcul.portugal.insmobile.dto.server.AccessPointFrequency;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.List;

public class AccessPointFrequencyRepository extends AsyncRepository<AccessPointFrequency> {

  public AccessPointFrequencyRepository(Context context) {
    Type type = new TypeToken<AccessPointFrequency>() {
    }.getType();
    Type typeCollection = new TypeToken<List<AccessPointFrequency>>() {
    }.getType();
    String url =
        Constants.PROTOCOL + "://" + Constants.HOST + ":" + Constants.PORT + "/"
            + ACCESS_POINT_FREQUENCY;
    super.start(context, AccessPointFrequencyRepository.class.getSimpleName(), type, typeCollection,
        url, false);
  }

}
