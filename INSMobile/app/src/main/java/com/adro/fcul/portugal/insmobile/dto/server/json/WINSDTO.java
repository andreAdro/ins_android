package com.adro.fcul.portugal.insmobile.dto.server.json;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;
import lombok.Data;

@Data
public class WINSDTO implements Serializable {

  private static final long serialVersionUID = 3474907275719292944L;

  private UUID identifier;
  private Date createdDate;
  private Date updatedDate;
}
