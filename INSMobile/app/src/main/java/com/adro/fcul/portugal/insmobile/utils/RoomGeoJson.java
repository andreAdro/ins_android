package com.adro.fcul.portugal.insmobile.utils;


import com.adro.fcul.portugal.insmobile.dto.server.entity.physical.Room;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class RoomGeoJson extends GeoJson {

  private final String type = "Feature";
  //  private final int id;
  private HashMap<String, String> properties = super.properties;
  private List<List<List<Double>>> coordinates;

  public RoomGeoJson(Room room) {
//    this.id = room.g();
    this.coordinates = getCoordinates(room);
    setLayerKey();
    setSourceKey();
  }

  private List<List<List<Double>>> getCoordinates(Room room) {
    List<List<List<Double>>> aList = new ArrayList<>();
//    for (Point point : room.getPolygon()
//        .getPoints()) {
//      List<Double> coordinates = new ArrayList<>();
//      double latitude = point.getLongitude();
//      double longitude = point.getLatitude();
//      coordinates.add(latitude);
//      coordinates.add(longitude);
//      aList.get(0)
//          .add(coordinates);
//    }
    return aList;
  }

  @Override
  public GeoJsonType getType() {
    return GeoJsonType.POLYGON;
  }

  @Override
  public String getGeoJsonObject() {
    return new Gson().toJson(this, RoomGeoJson.class);
  }

  private void setLayerKey() {
//    this.properties.put(GeoJsonLayerProperties.LAYER_ID, buildLayerId(this.id, "Room"));
  }

  private void setSourceKey() {
//    this.properties.put(GeoJsonLayerProperties.SOURCE_ID, buildSourceId(this.id, "Room"));
  }
}
