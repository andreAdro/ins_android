package com.adro.fcul.portugal.insmobile.dto.server.geo_json;

import com.adro.fcul.portugal.insmobile.dto.server.geo_json._enum.GeometryType;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GeometryDTO {

  @SerializedName("type")
  private GeometryType type;

  private Object coordinates;

  private PointDTO createCoordinate(ArrayList<Double> unparsedCoordinates) {
    Double longitude = unparsedCoordinates.get(0);
    Double latitude = unparsedCoordinates.get(1);
    Double altitude = 0.0;
    if (unparsedCoordinates.size() > 2) {
      altitude = unparsedCoordinates.get(2);
    }
    return new PointDTO(longitude, latitude, altitude);
  }

  public PointDTO getPointCoordinates() {
    if (type == GeometryType.POINT) {
      ArrayList<Double> coordinates = (ArrayList<Double>) this.coordinates;
      return createCoordinate(coordinates);
    }
    throw new RuntimeException(
        "Tried to convert a Geometry to its wrong representation. Check type to proceed");
  }

  public List<PointDTO> getLineCoordinates() {
    if (type == GeometryType.LINE_STRING
        || type == GeometryType.MULTI_POINT) {
      ArrayList<ArrayList<Double>> coordinates = (ArrayList<ArrayList<Double>>) this.coordinates;
      return extractLineCoordinates(coordinates);
    }
    throw new RuntimeException(
        "Tried to convert a Geometry to its wrong representation. Check type to proceed");
  }

  private List<PointDTO> extractLineCoordinates(ArrayList<ArrayList<Double>> coordinates) {
    List<PointDTO> parsedCoordinates = new ArrayList<>();
    for (ArrayList<Double> coordinate : coordinates) {
      parsedCoordinates.add(createCoordinate(coordinate));
    }
    return parsedCoordinates;
  }

  public List<List<PointDTO>> getPolygonCoordinates() {
    if (type == GeometryType.POLYGON
        || type == GeometryType.MULTI_LINE_STRING) {
      ArrayList<ArrayList<ArrayList<Double>>> coordinates = (ArrayList<ArrayList<ArrayList<Double>>>) this.coordinates;
      return extractPolygonCoordinates(coordinates);
    }
    throw new RuntimeException(
        "Tried to convert a Geometry to its wrong representation. Check type to proceed");
  }

  private List<List<PointDTO>> extractPolygonCoordinates(
      ArrayList<ArrayList<ArrayList<Double>>> coordinates) {
    List<List<PointDTO>> parsedCoordinates = new ArrayList<>(new ArrayList<>());
    for (ArrayList<ArrayList<Double>> coordinate : coordinates) {
      parsedCoordinates.add(extractLineCoordinates(coordinate));
    }
    return parsedCoordinates;
  }

  public List<List<List<PointDTO>>> getMultiPolygonCoordinates() {
    if (type == GeometryType.MULTI_POLYGON) {
      ArrayList<ArrayList<ArrayList<ArrayList<Double>>>> coordinates = (ArrayList<ArrayList<ArrayList<ArrayList<Double>>>>) this.coordinates;
      return extractMultiPolygonCoordinates(coordinates);
    }
    throw new RuntimeException(
        "Tried to convert a Geometry to its wrong representation. Check type to proceed");
  }

  private List<List<List<PointDTO>>> extractMultiPolygonCoordinates(
      ArrayList<ArrayList<ArrayList<ArrayList<Double>>>> coordinates) {
    List<List<List<PointDTO>>> parsedCoordinates = new ArrayList<>(new ArrayList<>());
    for (ArrayList<ArrayList<ArrayList<Double>>> coordinate : coordinates) {
      parsedCoordinates.add(extractPolygonCoordinates(coordinate));
    }
    return parsedCoordinates;
  }

}
