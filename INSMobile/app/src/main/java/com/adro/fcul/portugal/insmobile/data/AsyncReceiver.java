package com.adro.fcul.portugal.insmobile.data;

public interface AsyncReceiver {

  void receiveCallBack(String responseString);

}
