package com.adro.fcul.portugal.insmobile.activity.control_points_map_activity.control_points_activity;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.adro.fcul.portugal.insmobile.R;
import com.adro.fcul.portugal.insmobile.dto.server.json.FrequencyDTO;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

@Getter
public class ControlPointFrequencyRecyclerViewAdapter extends
    RecyclerView.Adapter<ControlPointPointFrequencyViewHolder> {

  private List<FrequencyDTO> accessPointFrequencies;

  ControlPointFrequencyRecyclerViewAdapter(
      List<FrequencyDTO> frequencies) {
    this.accessPointFrequencies = new ArrayList<>(frequencies);
  }

  @Override
  public ControlPointPointFrequencyViewHolder onCreateViewHolder(ViewGroup parent,
      int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.access_point_frequency_browse_item, parent, false);
    return new ControlPointPointFrequencyViewHolder(view);
  }

  @Override
  public void onBindViewHolder(ControlPointPointFrequencyViewHolder holder, int position) {
    FrequencyDTO frequency = this.accessPointFrequencies.get(position);
    holder.getBssidValue()
        .setText(String.format("%1s", frequency.getBssid()));
    holder.getRssiValue()
        .setText(String.format("%1s", frequency.getRssi()));
  }

  @Override
  public int getItemCount() {
    return this.accessPointFrequencies.size();
  }

  public void addItem(FrequencyDTO accessPointFrequency) {
    this.accessPointFrequencies.add(0, accessPointFrequency);
    notifyItemInserted(0);
  }

  public void resetWithNewData(List<FrequencyDTO> newAccessPointFrequencies) {
    int range = this.accessPointFrequencies.size() - newAccessPointFrequencies.size();
    notifyItemRangeRemoved(0, range);
    this.accessPointFrequencies = new ArrayList<>(newAccessPointFrequencies);
  }
}
