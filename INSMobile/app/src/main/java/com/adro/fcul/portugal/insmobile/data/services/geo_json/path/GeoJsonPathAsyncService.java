package com.adro.fcul.portugal.insmobile.data.services.geo_json.path;

import com.adro.fcul.portugal.insmobile.common.Constants;
import com.adro.fcul.portugal.insmobile.data.services.HttpAsyncCallback;
import java.net.MalformedURLException;
import java.net.URL;
import timber.log.Timber;

public abstract class GeoJsonPathAsyncService implements HttpAsyncCallback {

  protected URL url;

  public GeoJsonPathAsyncService() {
    try {
      String url = String.format("%s/%s", Constants.LOCATION, "navigation/paths");
      this.url = new URL(url);
    } catch (MalformedURLException e) {
      Timber.e(e);
      throw new RuntimeException("Unable to create a valid URL");
    }
  }
}
