package com.adro.fcul.portugal.insmobile.activity.search_activity.filter;

import android.support.v7.widget.RecyclerView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import com.adro.fcul.portugal.insmobile.dto.app.Searchable;
import java.util.ArrayList;
import java.util.List;

public class SearchViewListener implements OnQueryTextListener, SearchView.OnCloseListener {

  private List<Searchable> searchables;
  private RecyclerView recyclerView;
  private SearchRecyclerViewAdapter recyclerViewAdapter;

  public SearchViewListener(List<Searchable> searchables, RecyclerView recyclerView,
      SearchRecyclerViewAdapter recyclerViewAdapter) {
    this.searchables = searchables;
    this.recyclerView = recyclerView;
    this.recyclerViewAdapter = recyclerViewAdapter;
  }

  @Override
  public boolean onClose() {
    filterRecyclerView("");
    return false;
  }

  //TODO https://www.codementor.io/tips/1237823034/how-to-filter-a-recyclerview-with-a-searchview
  @Override
  public boolean onQueryTextChange(String query) {
    return filterRecyclerView(query);
  }

  @Override
  public boolean onQueryTextSubmit(String query) {
    return filterRecyclerView(query);
  }

  private boolean filterRecyclerView(String query) {
    query = query.toLowerCase();
    List<Searchable> filteredSearchables = new ArrayList<>();
    for (Searchable searchable : this.searchables) {
      if (searchable.getName()
          .toLowerCase()
          .contains(query)) {
        filteredSearchables.add(searchable);
      }
    }
    this.recyclerViewAdapter.animateTo(filteredSearchables);
    this.recyclerView.scrollToPosition(0);
    return true;
  }
}
