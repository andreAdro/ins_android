package com.adro.fcul.portugal.insmobile.activity.search_activity.filter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.adro.fcul.portugal.insmobile.R;
import com.adro.fcul.portugal.insmobile.dto.app.Searchable;
import java.util.ArrayList;
import java.util.List;

public class SearchRecyclerViewAdapter extends RecyclerView.Adapter<SearchableViewHolder> {

  private List<Searchable> searchables;

  public SearchRecyclerViewAdapter(List<Searchable> searchables) {
    this.searchables = new ArrayList<>(searchables);
  }

  @Override
  public SearchableViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.browse, parent, false);
    return new SearchableViewHolder(view);
  }

  @Override
  public void onBindViewHolder(SearchableViewHolder holder, int position) {
    Searchable searchable = searchables.get(position);
    holder.getName()
        .setText(searchable.getName());
  }

  @Override
  public int getItemCount() {
    return searchables.size();
  }

  private void removeItem(int position) {
    searchables.remove(position);
    notifyItemRemoved(position);
  }

  private void addItem(int position, Searchable searchable) {
    searchables.add(position, searchable);
    notifyItemInserted(position);
  }

  private void moveItem(int fromPosition, int toPosition) {
    Searchable searchable = searchables.remove(fromPosition);
    searchables.add(toPosition, searchable);
    notifyItemMoved(fromPosition, toPosition);
  }

  public void animateTo(List<Searchable> searchables) {
    applyAndAnimateRemovals(searchables);
    applyAndAnimateAdditions(searchables);
    applyAndAnimateMovedItems(searchables);
  }

  private void applyAndAnimateRemovals(List<Searchable> newSearchables) {
    for (int i = this.searchables.size() - 1; i >= 0; i--) {
      Searchable searchable = this.searchables.get(i);
      if (!newSearchables.contains(searchable)) {
        removeItem(i);
      }
    }
  }

  private void applyAndAnimateAdditions(List<Searchable> newSearchables) {
    for (int i = 0, count = newSearchables.size(); i < count; i++) {
      final Searchable model = newSearchables.get(i);
      if (!this.searchables.contains(model)) {
        addItem(i, model);
      }
    }
  }

  private void applyAndAnimateMovedItems(List<Searchable> newSearchables) {
    for (int toPosition = newSearchables.size() - 1; toPosition >= 0; toPosition--) {
      Searchable searchable = newSearchables.get(toPosition);
      int fromPosition = this.searchables.indexOf(searchable);
      if (fromPosition >= 0 && fromPosition != toPosition) {
        moveItem(fromPosition, toPosition);
      }
    }
  }

}
