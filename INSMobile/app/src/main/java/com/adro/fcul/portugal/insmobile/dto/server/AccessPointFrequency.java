package com.adro.fcul.portugal.insmobile.dto.server;

import com.adro.fcul.portugal.insmobile.dto.server.grid.Point;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AccessPointFrequency {

  @SerializedName("identifier")
  private Integer id;

  @SerializedName("bssid")
  private String bssid;

  @SerializedName("ssid")
  private String ssid;

  @SerializedName("rssi")
  private int rssi;

  @SerializedName("frequency")
  private int frequency;

  @SerializedName("point")
  private Point point;
}
