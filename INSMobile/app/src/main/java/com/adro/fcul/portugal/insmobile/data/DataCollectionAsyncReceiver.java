package com.adro.fcul.portugal.insmobile.data;

import java.util.List;

public interface DataCollectionAsyncReceiver<T> {

  void receiveData(List<T> t);
}
