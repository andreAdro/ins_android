package com.adro.fcul.portugal.insmobile.data;

public interface DataAsyncReceiver<T> {

  void receiveData(T t);

}
