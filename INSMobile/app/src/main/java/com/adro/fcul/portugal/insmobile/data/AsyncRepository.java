package com.adro.fcul.portugal.insmobile.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import com.google.gson.Gson;
import cz.msebera.android.httpclient.NameValuePair;
import java.lang.reflect.Type;
import java.util.List;

public abstract class AsyncRepository<T> extends AppCompatActivity implements AsyncReceiver {

  private static final String TAG = "AsyncRepository";
  private Context context;
  private String className;
  private Type type;
  private Type typeCollectionToken;
  private String url;
  private AsyncReceiver asyncReceiver;

  protected void start(Context context, String className, Type typeToken, Type typeCollectionToken,
      String url,
      boolean toFetchNow) {
    this.context = context;
    this.className = className;
    this.type = typeToken;
    this.typeCollectionToken = typeCollectionToken;
    this.url = url;
    if (toFetchNow) {
      refreshData();
    }
  }

  private void refreshData() {
    Log.d(TAG, "refreshData: starts");
    JSONAsyncTask task = new JSONAsyncTask(this, "GET");
    task.execute(this.url, "GET");
    //+ "http://10.101.226.63:8080/api/rooms", "GET");
  }

  @Override
  public void receiveCallBack(String responseString) {
    Log.d(TAG, "receiveCallBack: responseString -> " + responseString);
    saveCollectionAsStringToSharedPreferences(responseString);
    if (asyncReceiver != null) {
      asyncReceiver.receiveCallBack(responseString);
    }
  }

  public T getSingle() {
    refreshData();
    return getSingleFromSharedPrederences();
  }

  public List<T> getAll() {
    refreshData();
    return getCollectionFromSharedPreferences();
  }

  //TODO redo
  public void getById(String param, String value, AsyncReceiver asyncReceiver) {
    JSONAsyncTask task = new JSONAsyncTask(this, "GET");
    task.execute(this.url + "?" + param + "=" + value, "GET");
    this.asyncReceiver = asyncReceiver;
  }

  public void getByParameter(List<NameValuePair> parameters, AsyncReceiver asyncReceiver) {
    if (parameters.size() == 0) {
      throw new RuntimeException("getByParameters called without parameters.");
    }
    StringBuilder stringBuilder = new StringBuilder("?");
    for (NameValuePair param : parameters) {
      stringBuilder.append(String.format("%s=%s&", param.getName(), param.getValue()));
    }
    String string = stringBuilder.toString();
    JSONAsyncTask task = new JSONAsyncTask(this, "GET");
    task.execute(this.url + string.substring(0, string.length() - 1), "GET");

    this.asyncReceiver = asyncReceiver;
  }

  public void create(T t) {
    JSONAsyncTask task = new JSONAsyncTask(this, "POST", new Gson().toJson(t, this.type));
    task.execute(this.url, "POST");
  }

  private String getCollectionFromSharedPreferencesAsString() {
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(
        this.context);
    return sharedPreferences.getString(className, "");
  }

  private T getSingleFromSharedPrederences() {
    String collectionInString = getCollectionFromSharedPreferencesAsString();
    return new Gson().fromJson(collectionInString, this.type);
  }

  private List<T> getCollectionFromSharedPreferences() {
    String collectionInString = getCollectionFromSharedPreferencesAsString();
    return new Gson().fromJson(collectionInString, this.typeCollectionToken);
  }

  private void saveCollectionAsStringToSharedPreferences(String collection) {
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(
        this.context);
    sharedPreferences.edit()
        .putString(className, collection)
        .apply();
  }

  private void saveCollectionToSharedPreferences(List<T> collection) {
    String collectionInString = new Gson().toJson(collection);
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(
        this.context);
    sharedPreferences.edit()
        .putString(className, collectionInString)
        .apply();
  }

}
