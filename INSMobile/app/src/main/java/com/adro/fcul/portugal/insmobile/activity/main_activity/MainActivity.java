package com.adro.fcul.portugal.insmobile.activity.main_activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import com.adro.fcul.portugal.insmobile.R;
import com.adro.fcul.portugal.insmobile.activity.control_points_map_activity.ControlPointsMapActivity;
import com.adro.fcul.portugal.insmobile.activity.search_activity.SearchActivity;
import com.adro.fcul.portugal.insmobile.common.Constants.ActivityForResult;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.services.commons.models.Position;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

  private static final String TAG = "MainActivity";
  private DrawerLayout drawerLayout;
  private MapView mapView;
  private MapViewDataSync mapViewController;
  private Handler navigationHandler;
  private boolean isNavigating = false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    buildComponents(savedInstanceState);
  }

  private void buildComponents(Bundle savedInstanceState) {
    buildMap(savedInstanceState);
    buildToolbar();
    buildDrawer();
  }

  private void buildToolbar() {
    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    buildHamburguer();
  }

  private void buildHamburguer() {
    ActionBar actionbar = getSupportActionBar();
    if (actionbar != null) {
      actionbar.setDisplayHomeAsUpEnabled(true);
      actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
    }
  }

  private void buildDrawer() {
    drawerLayout = findViewById(R.id.drawer_layout);
    setNavigationViewOnItemSelectedListener();
    //  Add behaviours to drawer when opened
    //  drawerLayout.addDrawerListener(new DrawerListener() {});
  }

  private void buildMap(Bundle savedInstanceState) {
    Mapbox.getInstance(this, getString(R.string.mapBox_access_token));
    setContentView(R.layout.activity_main);
    List<Position> routeCoordinates;
    double longitude = -9.156660916489244;
    double latitude = 38.75723259537807;
    double longitude1 = -9.157487036856423;
    double latitude1 = 38.756938724794;
    routeCoordinates = new ArrayList<>();
    routeCoordinates.add(Position.fromCoordinates(longitude, latitude));
    routeCoordinates.add(Position.fromCoordinates(longitude1, latitude1));

    this.mapView = findViewById(R.id.mapView);

    this.mapView.onCreate(savedInstanceState);
    this.mapViewController = new MapViewDataSync(getApplicationContext());
    this.mapView.getMapAsync(mapViewController);
    this.navigationHandler = new Handler();
  }


  private void setNavigationViewOnItemSelectedListener() {
    NavigationView navigationView = findViewById(R.id.nav_view);
    navigationView.setNavigationItemSelectedListener(item -> {
      Log.d(TAG, "onNavigationItemSelected: starts");
      drawerLayout.closeDrawers();
      routeToActivity(item.getItemId());
      return true;
    });
  }

  // Handles all the activities created from MainActivity.
  // For startActivityForResult must update method this,onActivityResult to handle callback
  private void routeToActivity(int itemId) {
    switch (itemId) {
      case R.id.search_point_of_interest:
        Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
        startActivityForResult(intent, ActivityForResult.SEARCH_ACTIVITY);
        break;
      case R.id.control_point:
        Intent controlPointIntent = new Intent(getApplicationContext(),
            ControlPointsMapActivity.class);
        startActivityForResult(controlPointIntent, ActivityForResult.CONTROL_POINTS_MAP_ACTIVITY);
        break;
      default:
        break;
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    switch (requestCode) {
      case ActivityForResult.SEARCH_ACTIVITY:
        updateNavigation();
        break;
      case ActivityForResult.CONTROL_POINTS_MAP_ACTIVITY:
        //TODO do something
        break;
    }
    Log.d(TAG, "onActivityResult: resultCode" + requestCode);
  }

  // Extract destination from shared preferences and call method to create route @ MAP
  private void updateNavigation() {
    isNavigating = true;
    mapViewController.startNavigation();
//    int delay = 5000; // milliseconds -> 5 second
//    this.navigationHandler.postDelayed(new Runnable() {
//      @Override
//      public void run() {
//        if (isNavigating) {
//          mapViewController.startNavigation();
//        }
//        navigationHandler.postDelayed(this, delay);
//      }
//    }, delay);
  }


  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        drawerLayout.openDrawer(GravityCompat.START);
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onStart() {
    super.onStart();
    mapView.onStart();
  }

  @Override
  public void onResume() {
    super.onResume();
    mapView.onResume();
  }

  @Override
  public void onPause() {
    super.onPause();
    mapView.onPause();
  }

  @Override
  public void onStop() {
    super.onStop();
    mapView.onStop();
  }

  @Override
  public void onLowMemory() {
    super.onLowMemory();
    mapView.onLowMemory();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    mapView.onDestroy();
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    mapView.onSaveInstanceState(outState);
  }
}
