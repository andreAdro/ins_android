package com.adro.fcul.portugal.insmobile.dto.server.grid;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Point {

  @SerializedName("identifier")
  private Integer id;

  @SerializedName("latitude")
  private Double latitude;

  @SerializedName("longitude")
  private Double longitude;

  @SerializedName("height")
  private Double height;
}
