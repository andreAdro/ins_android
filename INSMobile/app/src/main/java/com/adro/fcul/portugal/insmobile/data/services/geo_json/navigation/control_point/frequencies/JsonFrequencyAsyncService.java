package com.adro.fcul.portugal.insmobile.data.services.geo_json.navigation.control_point.frequencies;

import com.adro.fcul.portugal.insmobile.common.Constants;
import com.adro.fcul.portugal.insmobile.data.services.HttpAsyncCallback;
import java.net.MalformedURLException;
import java.net.URL;
import timber.log.Timber;

public abstract class JsonFrequencyAsyncService implements HttpAsyncCallback {

  protected URL url;

  public JsonFrequencyAsyncService(String controlPointId) {
    try {
      String url = String.format("%s/%s/%s/%s", Constants.LOCATION, "navigation/control-points",
          controlPointId, "frequencies");
      this.url = new URL(url);
    } catch (MalformedURLException e) {
      Timber.e(e);
      throw new RuntimeException("Unable to create a valid URL");
    }
  }
}
