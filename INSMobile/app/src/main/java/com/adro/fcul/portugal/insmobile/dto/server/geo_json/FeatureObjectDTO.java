package com.adro.fcul.portugal.insmobile.dto.server.geo_json;

import com.adro.fcul.portugal.insmobile.dto.server.geo_json._enum.FeatureType;
import java.util.Map;
import java.util.UUID;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FeatureObjectDTO {

  private UUID id;
  private FeatureType type;
  private GeometryDTO geometry;
  private Map<String, Object> properties;
}
