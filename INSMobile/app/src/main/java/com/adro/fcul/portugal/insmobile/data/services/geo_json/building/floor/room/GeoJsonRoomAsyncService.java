package com.adro.fcul.portugal.insmobile.data.services.geo_json.building.floor.room;

import com.adro.fcul.portugal.insmobile.common.Constants;
import com.adro.fcul.portugal.insmobile.data.services.HttpAsyncCallback;
import java.net.MalformedURLException;
import java.net.URL;
import timber.log.Timber;

public abstract class GeoJsonRoomAsyncService implements HttpAsyncCallback {

  protected URL url;

  public GeoJsonRoomAsyncService(String buildingId, String floorId) {
    try {
      String url = String.format("%s/%s/%s/%s/%s/%s", Constants.LOCATION, "geo-json/buildings/",
          buildingId, "floors", floorId, "rooms");
      this.url = new URL(url);
    } catch (MalformedURLException e) {
      Timber.e(e);
      throw new RuntimeException("Unable to create a valid URL");
    }
  }

}
