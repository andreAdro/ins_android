package com.adro.fcul.portugal.insmobile.dto.server.entity.geometry;

import com.adro.fcul.portugal.insmobile.dto.server.entity.WINSEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
public class Point extends WINSEntity<Point> {

  private static final long serialVersionUID = 642488291861241230L;

  @Include
  private Double latitude;

  @Include
  private Double longitude;

  @Include
  private Double altitude;

  @Override
  public void update(Point point) {
    this.longitude = point.getLongitude();
    this.latitude = point.getLatitude();
    this.altitude = point.getAltitude();
  }
}
