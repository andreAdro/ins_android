package com.adro.fcul.portugal.insmobile.utils.mappers.geo_json.geometry;

import com.adro.fcul.portugal.insmobile.dto.server.entity.geometry.Polygon;
import com.adro.fcul.portugal.insmobile.dto.server.entity.geometry.Polyline;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json.FeatureObjectDTO;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json.GeometryDTO;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json.PointDTO;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json._enum.FeatureType;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json._enum.GeometryType;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class PolygonMapper extends PolylineMapper {

  protected Polygon geoJsonToPolygon(List<List<PointDTO>> polylineSegments, GeometryType type) {
    List<Polyline> polylines = new LinkedList<>();
    for (List<PointDTO> pointsSegment : polylineSegments) {
      Polyline polyline = super.geoJsonToPolyline(pointsSegment, GeometryType.LINE_STRING);
      polylines.add(polyline);
    }
    Polygon polygon = new Polygon();
    polygon.setPolylines(polylines);
    polygon.setGeometryType(type);
    return polygon;
  }

  protected List<List<Double[]>> polygonToGeoJsonCoordinates(Polygon polygon) {
    List<List<Double[]>> parsedCoordinates = new ArrayList<>();
    for (Polyline polyline : polygon.getPolylines()) {
      List<Double[]> coordinates = super.polylineToGeoJsonCoordinates(polyline);
      parsedCoordinates.add(coordinates);
    }
    return parsedCoordinates;
  }

  /**
   *
   */
  public FeatureObjectDTO polygonToGeoJson(Polygon building) {
    FeatureObjectDTO featureObjectDTO = new FeatureObjectDTO();
    extractCoordinates(building, featureObjectDTO);
    featureObjectDTO.setType(FeatureType.FEATURE);
    return featureObjectDTO;
  }

  private void extractCoordinates(Polygon polygon, FeatureObjectDTO featureObjectDTO) {
    if (null == featureObjectDTO.getGeometry()) {
      featureObjectDTO.setGeometry(new GeometryDTO());
    }
    Object parsedCoordinates = polygonToGeoJsonCoordinates(polygon);
    featureObjectDTO.getGeometry()
        .setCoordinates(parsedCoordinates);
    featureObjectDTO.getGeometry()
        .setType(polygon.getGeometryType());
  }

}
