package com.adro.fcul.portugal.insmobile.activity.control_points_map_activity;

import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillExtrusionBase;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillExtrusionColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillExtrusionHeight;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillExtrusionOpacity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;
import com.adro.fcul.portugal.insmobile.R;
import com.adro.fcul.portugal.insmobile.activity.control_points_map_activity.control_points_activity.ControlPointActivity;
import com.adro.fcul.portugal.insmobile.common.Constants.ActivityIntent;
import com.adro.fcul.portugal.insmobile.data.services.geo_json.building.GeoJsonBuildingsGETAsyncService;
import com.adro.fcul.portugal.insmobile.data.services.geo_json.building.floor.GeoJsonFloorGETAsyncService;
import com.adro.fcul.portugal.insmobile.data.services.geo_json.building.floor.room.GeoJsonRoomGETAsyncService;
import com.adro.fcul.portugal.insmobile.data.services.geo_json.navigation.control_point.GeoJsonControlPointsGETAsyncService;
import com.adro.fcul.portugal.insmobile.dto.server.PointOfInterest;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json.FeatureObjectDTO;
import com.adro.fcul.portugal.insmobile.dto.server.grid.Point;
import com.adro.fcul.portugal.insmobile.utils.mappers.json.PointOfInterestMapper;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.annotation.Symbol;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions;
import com.mapbox.mapboxsdk.style.layers.FillExtrusionLayer;
import com.mapbox.mapboxsdk.style.layers.Layer;
import com.mapbox.mapboxsdk.style.layers.Property;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.sources.GeoJsonOptions;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.mapboxsdk.style.sources.Source;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class ControlPointsMapActivity extends AppCompatActivity {

  private static final String TAG = "ControlPointsMapActivity";
  private MapView mapView;
  private MapboxMap mapboxMap;
  private SymbolManager symbolManager;
  private List<Symbol> symbols; // do not delete. it hold reference to rendered object on the jvm
  private HashMap<UUID, Layer> layers = new HashMap<>(); // do not delete. it hold reference to rendered object on the jvm

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    buildMap(savedInstanceState);
    activateToolbar(true);
  }

  private void buildMap(Bundle savedInstanceState) {
    Mapbox.getInstance(this, getString(R.string.mapBox_access_token));
    setContentView(R.layout.activity_control_points_map);
    this.mapView = findViewById(R.id.mapViewControlPoints);
    mapView.onCreate(savedInstanceState);
    mapView.getMapAsync(this::onMapReady);
  }

  public void onMapReady(MapboxMap mapboxMap) {
    this.mapboxMap = mapboxMap;
    setDefaultPosition();
    populateMapWithData();
  }

  private void setDefaultPosition() { // TODO set position based on obtained wifi position
    double longitude = -9.1573160;
    double latitude = 38.7569731;
    LatLng latLng = new LatLng();
    latLng.setLatitude(latitude);
    latLng.setLongitude(longitude);
    CameraPosition cameraPosition = new CameraPosition.Builder()
        .target(latLng)
        .zoom(19)
        .tilt(50)
        .bearing(67)
        .build();
    this.mapboxMap.setCameraPosition(cameraPosition);
    this.mapboxMap.getUiSettings()
        .setAllGesturesEnabled(true);
  }

  private void populateMapWithData() {
    addControlPointsToMap();
    this.mapboxMap.getStyle(this::addRoomsToMap);
  }

  private void addControlPointsToMap() {
    this.mapboxMap.setStyle(Style.MAPBOX_STREETS, style -> {
      new GeoJsonControlPointsGETAsyncService().GET(geoJsonControlPoints -> {
        Bitmap bitmap = generateBitmap(R.drawable.pinpoint);
        style.addImage("pinpoint", bitmap);

        GeoJsonOptions geoJsonOptions = new GeoJsonOptions().withTolerance(0.4f);
        this.symbolManager = new SymbolManager(mapView, mapboxMap, style, null, geoJsonOptions);
        symbolManager.addClickListener(this::onMakerClick);
        symbolManager.setIconAllowOverlap(true);
        symbolManager.setTextAllowOverlap(true);

        List<SymbolOptions> markersOptions = new ArrayList<>();
        for (FeatureObjectDTO geoJsonControlPoint : geoJsonControlPoints) {
          geoJsonToSymbolOptions(markersOptions, geoJsonControlPoint);
        }
        this.symbols = symbolManager.create(markersOptions);
      }, null);
    });
  }

  private void addRoomsToMap(Style style) {
    new GeoJsonBuildingsGETAsyncService().GET(buildingsGeoJson -> {
      for (FeatureObjectDTO buildingGeoJson : buildingsGeoJson) {
        new GeoJsonFloorGETAsyncService(buildingGeoJson.getId()
            .toString())
            .GET(floorsGeoJson -> {
              for (FeatureObjectDTO floorGeoJson : floorsGeoJson) {
                new GeoJsonRoomGETAsyncService(buildingGeoJson.getId()
                    .toString(), floorGeoJson.getId()
                    .toString())
                    .GET(roomsGeoJson -> {
                      for (FeatureObjectDTO roomGeoJson : roomsGeoJson) {
                        addLayer(style, roomGeoJson);
                      }
                    });
              }
            });
      }
    });
  }

  private void addLayer(Style style, FeatureObjectDTO featureObject) {
    String featureObjectId = featureObject.getId()
        .toString();
    String sourceKey = String.format("Source%s", featureObjectId);
    Source geoJsonSource = new GeoJsonSource(sourceKey, new Gson().toJson(featureObject));
    style.addSource(geoJsonSource);

    String layerKey = String.format("Layer%s", featureObjectId);
    FillExtrusionLayer layer = new FillExtrusionLayer(layerKey, sourceKey);
    layer.setProperties(
        fillExtrusionColor(Color.parseColor("#1841a0")),
        fillExtrusionBase(0f),
        fillExtrusionHeight(1f),
        fillExtrusionOpacity(1f),
        PropertyFactory.visibility(Property.VISIBLE)
    );
    style.addLayer(layer);
    layers.put(featureObject.getId(), layer);
  }

  private void geoJsonToSymbolOptions(List<SymbolOptions> markersOptions,
      FeatureObjectDTO geoJsonControlPoint) {
    LatLng latLng = extractLatLng(geoJsonControlPoint);
    SymbolOptions symbolOptions = new SymbolOptions()
        .withLatLng(latLng)
        .withIconImage("pinpoint")
        .withIconSize(1.3f)
        .withData(new Gson().toJsonTree(geoJsonControlPoint))
        .withSymbolSortKey(10.0f)
        .withDraggable(true);
    markersOptions.add(symbolOptions);
  }

  @NonNull
  private LatLng extractLatLng(FeatureObjectDTO geoJsonControlPoint) {
    com.adro.fcul.portugal.insmobile.dto.server.entity.physical.PointOfInterest pointOfInterest = new PointOfInterestMapper().geoJsonToPointOfInterest(
        geoJsonControlPoint);
    LatLng latLng = new LatLng();
    latLng.setLatitude(pointOfInterest.getLatitude());
    latLng.setLongitude(pointOfInterest.getLongitude());
    return latLng;
  }

  private void onMakerClick(Symbol symbol) {
    JsonElement jsonData = symbol.getData();
    String controlPointId = ((JsonObject) jsonData).get("id")
        .toString();

    Intent intent = new Intent(getApplicationContext(), ControlPointActivity.class);
    intent.addFlags(
        Intent.FLAG_ACTIVITY_NEW_TASK);// https://stackoverflow.com/questions/3689581/calling-startactivity-from-outside-of-an-activity
    intent.putExtra(ActivityIntent.CLICKED_MARKER_ID, controlPointId);
    Toast.makeText(getApplicationContext(), "Marker tapped: " + controlPointId,
        Toast.LENGTH_LONG)
        .show();
    getApplicationContext().startActivity(intent);
  }

  /**
   * Source code from https://github.com/mapbox/mapbox-plugins-android/blob/master/app/src/main/java/com/mapbox/mapboxsdk/plugins/testapp/activity/annotation/PressForSymbolActivity.java
   */
  private Bitmap generateBitmap(@DrawableRes int drawableRes) {
    Drawable drawable = getResources().getDrawable(drawableRes);
    return getBitmapFromDrawable(drawable);
  }

  static Bitmap getBitmapFromDrawable(Drawable drawable) {
    if (drawable instanceof BitmapDrawable) {
      return ((BitmapDrawable) drawable).getBitmap();
    } else {
      // width and height are equal for all assets since they are ovals.
      Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
          drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
      Canvas canvas = new Canvas(bitmap);
      drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
      drawable.draw(canvas);
      return bitmap;
    }
  }

  private MarkerOptions getMarkerOptionsFromPointOfInterest(PointOfInterest pointOfInterest) {
    Point point = pointOfInterest.getPoint();
    double latitude = point.getLatitude();
    double longitude = point.getLongitude();
    return new MarkerOptions().position(new LatLng(latitude, longitude))
        .title(Integer.toString(point.getId()))
        .snippet(pointOfInterest.getName());
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      onBackPressed();
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  private void activateToolbar(boolean enableHome) {
    ActionBar actionBar = getSupportActionBar();
    if (actionBar == null) {
      Toolbar toolbar = findViewById(R.id.toolbar);
      if (toolbar != null) {
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
      }
    }
    if (actionBar != null) {
      actionBar.setDisplayHomeAsUpEnabled(enableHome);
    }
  }
}