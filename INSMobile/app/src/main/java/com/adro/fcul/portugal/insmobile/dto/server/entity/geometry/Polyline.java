package com.adro.fcul.portugal.insmobile.dto.server.entity.geometry;

import com.adro.fcul.portugal.insmobile.dto.server.entity.WINSEntity;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json._enum.GeometryType;
import java.util.LinkedList;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
public class Polyline extends WINSEntity<Polyline> {

  private static final long serialVersionUID = 3080099235966569454L;

  private GeometryType geometryType;
  private List<Point> points = new LinkedList<>();

  @Override
  public void update(Polyline polyline) {
    this.geometryType = polyline.geometryType;
    this.points = polyline.getPoints();
  }
}
