package com.adro.fcul.portugal.insmobile.data.services.json.point_of_interest;

import com.adro.fcul.portugal.insmobile.data.DataCollectionAsyncReceiver;
import com.adro.fcul.portugal.insmobile.data.services.HttpGETAsyncService;
import com.adro.fcul.portugal.insmobile.dto.server.json.PointOfInterestDTO;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class PointsOfInterestAsyncGETService extends PointOfInterestAsyncService {

  private DataCollectionAsyncReceiver<PointOfInterestDTO> dataCollectionAsyncReceiver;

  public void GET(DataCollectionAsyncReceiver<PointOfInterestDTO> dataCollectionAsyncReceiver) {
    this.dataCollectionAsyncReceiver = dataCollectionAsyncReceiver;
    HttpGETAsyncService httpGetAsyncService =
        new HttpGETAsyncService(url, this);
    httpGetAsyncService.execute();
  }

  @Override
  public void callBack(String response) {
    this.dataCollectionAsyncReceiver.receiveData(jsonToPointsOfInterest(response));
  }

  private List<PointOfInterestDTO> jsonToPointsOfInterest(String json) {
    Type listType = new TypeToken<ArrayList<PointOfInterestDTO>>() {
    }.getType();
    return new Gson().fromJson(json, listType);
  }
}
