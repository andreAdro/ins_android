package com.adro.fcul.portugal.insmobile.data.services.geo_json.path;

import com.adro.fcul.portugal.insmobile.data.DataAsyncReceiver;
import com.adro.fcul.portugal.insmobile.data.services.HttpGETAsyncService;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json.FeatureObjectDTO;
import com.google.gson.Gson;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;
import timber.log.Timber;

public class GeoJsonPathGETAsyncServiceImpl extends GeoJsonPathAsyncService {

  private DataAsyncReceiver<FeatureObjectDTO> dataAsyncReceiver;

  public void GET(UUID controlPointId,
      UUID pointOfInterestId,
      DataAsyncReceiver<FeatureObjectDTO> dataAsyncReceiver) {
    this.dataAsyncReceiver = dataAsyncReceiver;
    URL url = buildUrlWithId(controlPointId, pointOfInterestId);
    HttpGETAsyncService httpGetAsyncService = new HttpGETAsyncService(url, this);
    httpGetAsyncService.execute();
  }

  private URL buildUrlWithId(UUID controlPointId, UUID pointOfInterestId) {
    try {
      return new URL(
          String.format(
              "%s?controlPointId=%s&pointOfInterestId=%s",
              this.url.toExternalForm(),
              controlPointId,
              pointOfInterestId
          )
      );
    } catch (MalformedURLException e) {
      Timber.e(e);
      throw new RuntimeException("Unable to create a valid URL");
    }
  }

  @Override
  public void callBack(String response) {
    this.dataAsyncReceiver.receiveData(jsonToFeatureObject(response));
  }

  private FeatureObjectDTO jsonToFeatureObject(String json) {
    return new Gson().fromJson(json, FeatureObjectDTO.class);
  }
}
