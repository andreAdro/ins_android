package com.adro.fcul.portugal.insmobile.data.services.geo_json.navigation.control_point.frequencies;

import com.adro.fcul.portugal.insmobile.data.DataCollectionAsyncReceiver;
import com.adro.fcul.portugal.insmobile.data.services.HttpGETAsyncService;
import com.adro.fcul.portugal.insmobile.dto.server.json.FrequencyDTO;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class JsonFrequenciesGETAsyncService extends JsonFrequencyAsyncService {

  private DataCollectionAsyncReceiver<FrequencyDTO> dataAsyncReceiver;

  public JsonFrequenciesGETAsyncService(String controlPointId) {
    super(controlPointId);
  }

  public void GET(DataCollectionAsyncReceiver<FrequencyDTO> dataCollectionAsyncReceiver) {
    this.dataAsyncReceiver = dataCollectionAsyncReceiver;
    new HttpGETAsyncService(super.url, this).execute();
  }

  @Override
  public void callBack(String response) {
    this.dataAsyncReceiver.receiveData(jsonToPoint(response));
  }

  private List<FrequencyDTO> jsonToPoint(String json) {
    Type listType = new TypeToken<ArrayList<FrequencyDTO>>() {
    }.getType();
    return new Gson().fromJson(json, listType);
  }

}
