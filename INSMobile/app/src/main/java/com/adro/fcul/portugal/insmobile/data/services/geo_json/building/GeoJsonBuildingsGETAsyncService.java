package com.adro.fcul.portugal.insmobile.data.services.geo_json.building;

import com.adro.fcul.portugal.insmobile.data.DataAsyncReceiver;
import com.adro.fcul.portugal.insmobile.data.services.HttpGETAsyncService;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json.FeatureObjectDTO;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class GeoJsonBuildingsGETAsyncService extends GeoJsonBuildingAsyncService {

  private DataAsyncReceiver<List<FeatureObjectDTO>> dataAsyncReceiver;

  public void GET(DataAsyncReceiver<List<FeatureObjectDTO>> dataAsyncReceiver) {
    this.dataAsyncReceiver = dataAsyncReceiver;
    HttpGETAsyncService httpGetAsyncService = new HttpGETAsyncService(url, this);
    httpGetAsyncService.execute();
  }

  @Override
  public void callBack(String response) {
    this.dataAsyncReceiver.receiveData(jsonToFeatureObject(response));
  }

  private List<FeatureObjectDTO> jsonToFeatureObject(String json) {
    Type listType = new TypeToken<ArrayList<FeatureObjectDTO>>() {
    }.getType();
    return new Gson().fromJson(json, listType);
  }
}
