package com.adro.fcul.portugal.insmobile.dto.server.geo_json;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PointDTO {

  private Double longitude;
  private Double latitude;
  private Double altitude;
}
