package com.adro.fcul.portugal.insmobile.dto.server.geo_json_TODELETE;

import com.adro.fcul.portugal.insmobile.dto.server.grid.Polyline;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class RouteGeoJson extends GeoJson {

  private PolylineGeometry geometry;

  public RouteGeoJson(Polyline polyline) {
    this.type = "Feature";
    this.geometry = getGeometry(polyline);
    this.properties = getProperties(polyline);
  }

  private PolylineGeometry getGeometry(Polyline polyline) {
    PolylineGeometry geometry = new PolylineGeometry();
    geometry.setType("LineString");
    List<Double> startPoints = new ArrayList<>();
    startPoints.add(polyline.getStart()
        .getLongitude());
    startPoints.add(polyline.getStart()
        .getLatitude());
    geometry.getCoordinates()
        .add(startPoints);
    List<Double> endPoints = new ArrayList<>();
    endPoints.add(polyline.getEnd()
        .getLongitude());
    endPoints.add(polyline.getEnd()
        .getLatitude());
    geometry.getCoordinates()
        .add(endPoints);
    return geometry;
  }

  private HashMap<String, String> getProperties(Polyline polyline) {
    HashMap<String, String> properties = new HashMap<>();
    properties.put("name", String.valueOf(polyline.getId()));
    return properties;
  }

  @Data
  public class PolylineGeometry {

    private String type;
    private List<List<Double>> coordinates = new ArrayList<>();
  }
}
