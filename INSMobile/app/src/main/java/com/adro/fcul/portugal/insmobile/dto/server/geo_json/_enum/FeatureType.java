package com.adro.fcul.portugal.insmobile.dto.server.geo_json._enum;

import com.google.gson.annotations.SerializedName;

public enum FeatureType {

  @SerializedName("Feature")
  FEATURE,
  @SerializedName("FeatureCollection")
  FEATURE_COLLECTION
}
