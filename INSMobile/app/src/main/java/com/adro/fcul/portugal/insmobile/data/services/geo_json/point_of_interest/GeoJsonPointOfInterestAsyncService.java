package com.adro.fcul.portugal.insmobile.data.services.geo_json.point_of_interest;

import com.adro.fcul.portugal.insmobile.common.Constants;
import com.adro.fcul.portugal.insmobile.data.services.HttpAsyncCallback;
import java.net.MalformedURLException;
import java.net.URL;
import timber.log.Timber;

public abstract class GeoJsonPointOfInterestAsyncService implements HttpAsyncCallback {

  protected URL url;

  public GeoJsonPointOfInterestAsyncService() {
    try {
      String url = String.format("%s/%s", Constants.LOCATION, "geo-json/points-of-interest");
      this.url = new URL(url);
    } catch (MalformedURLException e) {
      Timber.e(e);
      throw new RuntimeException("Unable to create a valid URL");
    }
  }
}
