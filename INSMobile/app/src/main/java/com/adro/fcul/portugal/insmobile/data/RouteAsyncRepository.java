package com.adro.fcul.portugal.insmobile.data;

import android.content.Context;
import com.adro.fcul.portugal.insmobile.common.Constants;
import com.adro.fcul.portugal.insmobile.common.Constants.EndpointConstants;
import com.adro.fcul.portugal.insmobile.dto.server.grid.Point;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.List;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class RouteAsyncRepository extends AsyncRepository<Point> {

  public RouteAsyncRepository(Context context) {
    //  https://stackoverflow.com/questions/32444863/google-gson-linkedtreemap-class-cast-to-myclass/32494079#32494079
    Type type = new TypeToken<Point>() {
    }.getType();
    Type typeCollection = new TypeToken<List<Point>>() {
    }.getType();
    String url =
        Constants.PROTOCOL + "://" + Constants.HOST + ":" + Constants.PORT
            + EndpointConstants.ROUTE_POLYLINES;
    super.start(context, RouteAsyncRepository.class.getSimpleName(), type, typeCollection, url,
        false);
  }
}
