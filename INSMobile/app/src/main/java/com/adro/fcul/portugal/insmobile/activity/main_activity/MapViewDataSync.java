package com.adro.fcul.portugal.insmobile.activity.main_activity;

import static com.mapbox.mapboxsdk.Mapbox.getApplicationContext;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillExtrusionBase;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillExtrusionColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillExtrusionHeight;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillExtrusionOpacity;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineColor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.widget.Toast;
import com.adro.fcul.portugal.insmobile.data.services.geo_json.building.GeoJsonBuildingsGETAsyncService;
import com.adro.fcul.portugal.insmobile.data.services.geo_json.building.floor.GeoJsonFloorGETAsyncService;
import com.adro.fcul.portugal.insmobile.data.services.geo_json.building.floor.room.GeoJsonRoomGETAsyncService;
import com.adro.fcul.portugal.insmobile.data.services.geo_json.navigation.control_point.GeoJsonControlPointsGETAsyncService;
import com.adro.fcul.portugal.insmobile.data.services.geo_json.path.GeoJsonPathGETAsyncServiceImpl;
import com.adro.fcul.portugal.insmobile.data.services.json.point_of_interest.room.POIRoomsAsyncGETServiceImpl;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json.FeatureObjectDTO;
import com.adro.fcul.portugal.insmobile.dto.server.json.FrequencyDTO;
import com.adro.fcul.portugal.insmobile.dto.server.json.RoomDTO;
import com.adro.fcul.portugal.insmobile.utils.NavigationUtil;
import com.adro.fcul.portugal.insmobile.utils.WifiUtil;
import com.google.gson.Gson;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.FillExtrusionLayer;
import com.mapbox.mapboxsdk.style.layers.Layer;
import com.mapbox.mapboxsdk.style.layers.LineLayer;
import com.mapbox.mapboxsdk.style.layers.Property;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.mapboxsdk.style.sources.Source;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class MapViewDataSync implements OnMapReadyCallback {

  private Context context;
  private MapboxMap mapboxMap;
  private HashMap<UUID, Layer> layers = new HashMap<>();
  private List<Layer> highLightLayers = new ArrayList<>();
  private Source pathSource;
  private LineLayer pathLayer;
  private BroadcastReceiver wifiBroadcastReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      updateNavigation();
    }
  };

  public MapViewDataSync(Context context) {
    this.context = context;
  }

  @Override
  public void onMapReady(MapboxMap mapboxMap) {
    this.mapboxMap = mapboxMap;
    setDefaultPosition();
    drawLayers();
  }

  private void setDefaultPosition() { // TODO set position based on obtained wifi position
    double longitude = -9.1573160;
    double latitude = 38.7569731;
    LatLng latLng = new LatLng();
    latLng.setLatitude(latitude);
    latLng.setLongitude(longitude);
    CameraPosition cameraPosition = new CameraPosition.Builder()
        .target(latLng)
        .zoom(19)
        .tilt(50)
        .bearing(67)
        .build();
    this.mapboxMap.setCameraPosition(cameraPosition);
    this.mapboxMap.getUiSettings()
        .setAllGesturesEnabled(true);
  }

  private void drawLayers() {
    this.mapboxMap.setStyle(Style.MAPBOX_STREETS, style ->
        new GeoJsonBuildingsGETAsyncService().GET(buildingsGeoJson -> {
          for (FeatureObjectDTO buildingGeoJson : buildingsGeoJson) {
            addLayer(style, buildingGeoJson, true, "#11192c", 1f);
            new GeoJsonFloorGETAsyncService(buildingGeoJson.getId()
                .toString())
                .GET(floorsGeoJson -> {
                  for (FeatureObjectDTO floorGeoJson : floorsGeoJson) {
                    addLayer(style, floorGeoJson, true, "#1f3770", 2f);
                    new GeoJsonRoomGETAsyncService(buildingGeoJson.getId()
                        .toString(), floorGeoJson.getId()
                        .toString())
                        .GET(roomsGeoJson -> {
                          for (FeatureObjectDTO roomGeoJson : roomsGeoJson) {
                            addLayer(style, roomGeoJson, true, "#1841a0", 4f);
                          }
                        });
                  }
                });
          }
        })
    );
  }

  private void addLayer(Style style, FeatureObjectDTO featureObject,
      boolean isVisible,
      String colorCode,
      Float height) {
    String featureObjectId = featureObject.getId()
        .toString();
    String sourceKey = String.format("Source%s", featureObjectId);
    Source geoJsonSource = new GeoJsonSource(sourceKey, new Gson().toJson(featureObject));
    style.addSource(geoJsonSource);

    String layerKey = String.format("Layer%s", featureObjectId);
    FillExtrusionLayer layer = new FillExtrusionLayer(layerKey, sourceKey);
    layer.setProperties(
        fillExtrusionColor(Color.parseColor(colorCode)),
        fillExtrusionBase(0f),
        fillExtrusionHeight(height),
        fillExtrusionOpacity(1f),
        PropertyFactory.visibility(isVisible ? Property.VISIBLE : Property.NONE)
    );
    style.addLayer(layer);
    layers.put(featureObject.getId(), layer);
  }

  void startNavigation() {
    startScanning();
  }

  private void startScanning() {
    this.context.registerReceiver(
        wifiBroadcastReceiver,
        new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)
    );
    WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(
        Context.WIFI_SERVICE);
    wifiManager.startScan();
  }

  private void updateNavigation() {
    List<FrequencyDTO> frequencies = getFrequenciesFromWifiManagement();
    new GeoJsonControlPointsGETAsyncService().GET(geoJsonControlPoints -> {
      UUID controlPointId = UUID.fromString("c0a80108-6cfe-1af2-816c-fe5b05550001");
      if (null != geoJsonControlPoints
          && !geoJsonControlPoints.isEmpty()) {
        controlPointId = geoJsonControlPoints.get(0)
            .getId();
      }
      Toast.makeText(getApplicationContext(), "Refreshed navigation. Starting @: " + controlPointId,
          Toast.LENGTH_LONG)
          .show();
      FeatureObjectDTO goal = NavigationUtil.getGoal(this.context);

      UUID pointOfInterestId = goal.getId();
      animateRooms(pointOfInterestId);
      animateRoute(controlPointId, pointOfInterestId);
      WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(
          Context.WIFI_SERVICE);
      wifiManager.startScan();
    }, frequencies);
//    // TODO switch with current position after network is parametrized
//    FeatureObjectDTO start = NavigationUtil.getStart(this.context);
//    FeatureObjectDTO goal = NavigationUtil.getGoal(this.context);
//
//    UUID pointOfInterestId = goal.getId();
//    animateRooms(pointOfInterestId);
////    UUID controlPointId = start.getId(); // TODO uncomment when position is obtained from WI-FI
//    UUID controlPointId = UUID.fromString("c0a80108-6cf8-112e-816c-f8c142fc0001");
//    animateRoute(controlPointId, pointOfInterestId);
  }


  private List<FrequencyDTO> getFrequenciesFromWifiManagement() {
    List<FrequencyDTO> newFrequencies = new ArrayList<>();
    WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(
        Context.WIFI_SERVICE);
    List<ScanResult> wifiList = wifiManager.getScanResults();
    for (ScanResult scanResult : wifiList) {
      if (scanResult.level > -75) {
        FrequencyDTO accessPointFrequency = WifiUtil.getFrequencyFromScanResult(
            scanResult);
        newFrequencies.add(accessPointFrequency);
      }
    }
    return newFrequencies;
  }

  private void animateRoute(UUID controlPointId, UUID pointOfInterestId) {
    new GeoJsonPathGETAsyncServiceImpl().GET(
        controlPointId,
        pointOfInterestId,
        this::addLayerRoute
    );
  }

  private void addLayerRoute(FeatureObjectDTO geoJson) {
    this.mapboxMap.getStyle(style -> {
      // Removed previous path
      String sourceKey = String.format("Source%s", UUID.randomUUID()
          .toString());
      String layerKey = String.format("Layer%s", UUID.randomUUID()
          .toString());
      if (null != this.pathSource && null != pathLayer) {
        style.removeSource(pathSource);
        style.removeLayer(pathLayer);
      }

      Source geoJsonSource = new GeoJsonSource(sourceKey, new Gson().toJson(geoJson));

      style.addSource(geoJsonSource);

      LineLayer lineLayer = new LineLayer(layerKey, sourceKey);
      lineLayer.setProperties(
          lineColor(Color.parseColor("#FFFFE0")),
          PropertyFactory.lineCap(Property.LINE_CAP_SQUARE),
          PropertyFactory.lineJoin(Property.LINE_JOIN_MITER),
          PropertyFactory.lineOpacity(.8f),
          PropertyFactory.lineWidth(2f),
//          PropertyFactory.textLineHeight(4f), // doesn't work and there's no workaround with mapbox sdk
          PropertyFactory.visibility(Property.VISIBLE)
      );
      style.addLayer(lineLayer);
      this.pathSource = geoJsonSource;
      this.pathLayer = lineLayer;
    });
  }


  private void animateRooms(UUID targetLayerId) {
    new POIRoomsAsyncGETServiceImpl(targetLayerId.toString()).GET(rooms -> {
      removeHighlights();
      addHighlights(rooms);
    });
  }

  private void removeHighlights() {
    for (Layer highLightLayer : this.highLightLayers) {
      highLightLayer.setProperties(
          fillExtrusionHeight(3f),
          fillExtrusionColor(Color.parseColor("#1841a0"))
      );
    }
  }

  private void addHighlights(List<RoomDTO> rooms) {
    for (RoomDTO room : rooms) {
      Layer layer = this.layers.get(room.getIdentifier());
      if (null != layer) {
        layer.setProperties(
            fillExtrusionHeight(6f),
            fillExtrusionColor(Color.parseColor("#94DEFE"))
        );
        this.highLightLayers.add(layer);
      }
    }
  }
}
