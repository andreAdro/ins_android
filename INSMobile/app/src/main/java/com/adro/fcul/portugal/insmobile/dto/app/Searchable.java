package com.adro.fcul.portugal.insmobile.dto.app;

import com.adro.fcul.portugal.insmobile.dto.server.json.PointOfInterestDTO;
import java.util.UUID;
import lombok.Data;

@Data
public class Searchable {

  private UUID identifier;
  private String name;
  private PointOfInterestDTO pointOfInterest;

  public Searchable(PointOfInterestDTO pointOfInterest) {
    this.identifier = pointOfInterest.getIdentifier();
    this.name = pointOfInterest.getName();
    this.pointOfInterest = pointOfInterest; // TODO remove
  }
}
