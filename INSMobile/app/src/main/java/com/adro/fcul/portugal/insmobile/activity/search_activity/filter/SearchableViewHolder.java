package com.adro.fcul.portugal.insmobile.activity.search_activity.filter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.adro.fcul.portugal.insmobile.R;
import lombok.Getter;

@Getter
class SearchableViewHolder extends RecyclerView.ViewHolder {

  private static final String TAG = "SearchableViewHolder";

  private final ImageView image;
  private final TextView name;

  SearchableViewHolder(View view) {
    super(view);
    this.image = view.findViewById(R.id.searchable_image);
    this.name = view.findViewById(R.id.searchable_name);
  }

}
