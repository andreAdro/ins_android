package com.adro.fcul.portugal.insmobile.utils.mappers.geo_json;

import com.adro.fcul.portugal.insmobile.dto.server.entity.geometry.Polygon;
import com.adro.fcul.portugal.insmobile.dto.server.entity.physical.PointOfInterest;
import com.adro.fcul.portugal.insmobile.dto.server.entity.physical.Room;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json.FeatureObjectDTO;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json.GeometryDTO;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json._enum.FeatureType;
import com.adro.fcul.portugal.insmobile.utils.mappers.geo_json.geometry.PolygonMapper;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class RoomMapper extends PolygonMapper {

  protected PropertiesMapper propertiesMapper;

  public RoomMapper() {
    this.propertiesMapper = new PropertiesMapper();
  }

  /**
   *
   */
  public Room geoJsonToRoom(FeatureObjectDTO featureObjectDTO) {
    Room room = new Room();
    appendWinsEntityData(room, featureObjectDTO);
    appendCoordinates(room, featureObjectDTO);
    appendProperties(room, featureObjectDTO);
    return room;
  }

  private void appendCoordinates(Room room, FeatureObjectDTO featureObjectDTO) {
    Polygon polygon = super.geoJsonToPolygon(
        featureObjectDTO.getGeometry()
            .getPolygonCoordinates(),
        featureObjectDTO.getGeometry()
            .getType()
    );
    room.update(polygon);
  }

  private void appendProperties(Room room, FeatureObjectDTO featureObjectDTO) {
    Map<String, Object> properties = featureObjectDTO.getProperties();
    appendName(room, properties);
    appendPointsOfInterest(room, properties);
  }

  private void appendName(Room room, Map<String, Object> properties) {
    Object roomName = properties.get("name");
    if (roomName instanceof String) {
      room.setName((String) roomName);
    }
  }

  private void appendPointsOfInterest(Room room, Map<String, Object> properties) {
    Object pointsOfInterestArray = properties.get("pointsOfInterest");
    Set<PointOfInterest> pointOfInterests =
        this.propertiesMapper.propertiesToPointsOfInterest(pointsOfInterestArray);
    room.setPointsOfInterest(pointOfInterests);
  }

  /**
   *
   */
  public FeatureObjectDTO roomToGeoJson(Room room) {
    FeatureObjectDTO featureObjectDTO = new FeatureObjectDTO();
    extractCoordinates(room, featureObjectDTO);
    extractProperties(room, featureObjectDTO);
    extractOtherData(room, featureObjectDTO);
    return featureObjectDTO;
  }


  private void extractCoordinates(Room room, FeatureObjectDTO featureObjectDTO) {
    if (null == featureObjectDTO.getGeometry()) {
      featureObjectDTO.setGeometry(new GeometryDTO());
    }
    Object parsedCoordinates = super.polygonToGeoJsonCoordinates(room);
    featureObjectDTO.getGeometry()
        .setCoordinates(parsedCoordinates);
    featureObjectDTO.getGeometry()
        .setType(room.getGeometryType());
  }


  private void extractProperties(Room room, FeatureObjectDTO featureObjectDTO) {
    Map<String, Object> properties = new HashMap<>();
    extractName(room, properties);
    extractPointsOfInterest(room, properties);
    featureObjectDTO.setProperties(properties);
  }

  private void extractName(Room room, Map<String, Object> properties) {
    properties.put("name", room.getName());
  }

  private void extractPointsOfInterest(Room room, Map<String, Object> properties) {
    this.propertiesMapper
        .extractPointsOfInterestToProperties(room.getPointsOfInterest(), properties);
  }

  private void extractOtherData(Room room, FeatureObjectDTO featureObjectDTO) {
    featureObjectDTO.setType(FeatureType.FEATURE);
    featureObjectDTO.setId(room.getIdentifier());
  }

}
