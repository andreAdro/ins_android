package com.adro.fcul.portugal.insmobile.utils.mappers.geo_json.geometry;

import com.adro.fcul.portugal.insmobile.dto.server.entity.geometry.Point;
import com.adro.fcul.portugal.insmobile.dto.server.entity.geometry.Polyline;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json.PointDTO;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json._enum.GeometryType;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public abstract class PolylineMapper extends PointMapper {

  protected Polyline geoJsonToPolyline(List<PointDTO> pointsSegment, GeometryType type) {
    List<Point> points = new LinkedList<>();
    for (PointDTO wallCoordinate : pointsSegment) {
      Point point = super.geoJsonCoordinateToPoint(wallCoordinate);
      points.add(point);
    }
    Polyline polyline = new Polyline();
    polyline.setPoints(points);
    polyline.setGeometryType(type);
    return polyline;
  }

  protected List<Double[]> polylineToGeoJsonCoordinates(Polyline polyline) {
    List<Double[]> parsedCoordinates = new ArrayList<>();
    for (Point point : polyline.getPoints()) {
      Double[] coordinate = super.pointToGeoJsonCoordinates(point);
      parsedCoordinates.add(coordinate);
    }
    return parsedCoordinates;
  }

}
