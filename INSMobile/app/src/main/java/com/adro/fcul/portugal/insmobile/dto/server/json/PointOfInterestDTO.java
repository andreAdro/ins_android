package com.adro.fcul.portugal.insmobile.dto.server.json;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class PointOfInterestDTO extends WINSDTO {

  private static final long serialVersionUID = -201378892625993636L;

  private String name;
}
