package com.adro.fcul.portugal.insmobile.data.services;

public interface HttpAsyncCallback {

  void callBack(String response);

}
