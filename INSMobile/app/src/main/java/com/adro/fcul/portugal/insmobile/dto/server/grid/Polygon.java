package com.adro.fcul.portugal.insmobile.dto.server.grid;

import com.google.gson.annotations.SerializedName;
import java.util.Set;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Polygon {

  @SerializedName("identifier")
  private Integer id;

  @SerializedName("points")
  private Set<Point> points;
}
