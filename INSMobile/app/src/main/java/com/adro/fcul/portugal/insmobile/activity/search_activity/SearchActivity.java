package com.adro.fcul.portugal.insmobile.activity.search_activity;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import com.adro.fcul.portugal.insmobile.R;
import com.adro.fcul.portugal.insmobile.activity.search_activity.action.SearchableItemClickListener;
import com.adro.fcul.portugal.insmobile.activity.search_activity.filter.SearchRecyclerViewAdapter;
import com.adro.fcul.portugal.insmobile.activity.search_activity.filter.SearchViewListener;
import com.adro.fcul.portugal.insmobile.data.services.json.point_of_interest.PointsOfInterestAsyncGETService;
import com.adro.fcul.portugal.insmobile.dto.app.Searchable;
import com.adro.fcul.portugal.insmobile.dto.server.json.PointOfInterestDTO;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SearchActivity extends AppCompatActivity {

  private static final String TAG = "SearchActivity";
  private SearchView searchView;
  private RecyclerView recyclerView;
  private SearchRecyclerViewAdapter adapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_search_for);
    activateToolbar(true);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_search, menu);

    SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
    this.searchView = (SearchView) menu.findItem(R.id.app_bar_search)
        .getActionView();
    SearchableInfo searchableInfo = searchManager.getSearchableInfo(getComponentName());
    this.searchView.setSearchableInfo(searchableInfo);
    this.searchView.setIconified(true);
    // Fetches needed data for this activity context
    new PointsOfInterestAsyncGETService().GET(this::receiveData);
    return true;
  }

  public void receiveData(List<PointOfInterestDTO> pointsOfInterest) {
    List<Searchable> searchables = new ArrayList<>();
    for (PointOfInterestDTO pointOfInterest : pointsOfInterest) {
      searchables.add(new Searchable(pointOfInterest));
    }
    Collections.sort(searchables, (o1, o2) -> o1.getName()
        .compareTo(o2.getName()));
    populateMenuWithData(searchables);
  }

  private void populateMenuWithData(List<Searchable> searchables) {
    // Creates recycler view
    createRecyclerView(searchables, this.searchView);

    // Creates listener for searchView behaviours
    SearchViewListener searchViewListener = new SearchViewListener(searchables,
        this.recyclerView, this.adapter);
    // Sets listener for searchView behavious
    this.searchView.setOnQueryTextListener(searchViewListener);
    this.searchView.setOnCloseListener(searchViewListener);
  }

  private void createRecyclerView(List<Searchable> searchables, SearchView searchView) {
    this.recyclerView = findViewById(R.id.recycler_view);
    recyclerView.setLayoutManager(new LinearLayoutManager(this));
    recyclerView.addOnItemTouchListener(
        new SearchableItemClickListener(getApplicationContext(), recyclerView, searchView,
            searchables, this));
    adapter = new SearchRecyclerViewAdapter(searchables);
    recyclerView.setAdapter(adapter);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      onBackPressed();
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  private void activateToolbar(boolean enableHome) {
    Log.d(TAG, "activateToolbar: starts");
    ActionBar actionBar = getSupportActionBar();
    if (actionBar == null) {
      Toolbar toolbar = findViewById(R.id.toolbar);
      if (toolbar != null) {
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
      }
    }
    if (actionBar != null) {
      actionBar.setDisplayHomeAsUpEnabled(enableHome);
    }
  }
}
