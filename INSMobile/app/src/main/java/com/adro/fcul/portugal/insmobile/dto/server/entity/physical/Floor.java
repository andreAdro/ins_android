package com.adro.fcul.portugal.insmobile.dto.server.entity.physical;

import com.adro.fcul.portugal.insmobile.dto.server.entity.geometry.Polygon;
import com.adro.fcul.portugal.insmobile.dto.server.geo_json._enum.FloorType;
import java.util.Set;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
public class Floor extends Polygon {

  private static final long serialVersionUID = 158749745384860337L;

  private FloorType type;
  private Building building;
  private Set<Room> rooms;
  private Set<PointOfInterest> pointsOfInterest;

  public void update(Floor floor) {
    super.update(floor);
    this.type = floor.getType();
    this.building = floor.getBuilding();
    this.rooms = floor.getRooms();
    this.pointsOfInterest = building.getPointsOfInterest();
  }
}
